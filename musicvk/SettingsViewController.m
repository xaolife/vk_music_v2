//
//  SettingsViewController.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 26/04/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "SettingsViewController.h"
#import "LoginViewController.h"
#import "AboutViewController.h"
#import "VkUser.h"
#import "VKApi.h"
#import <MessageUI/MessageUI.h>

#import "iRate.h"

@interface SettingsViewController () <MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) UITableViewCell *signInCell;

@property (strong, nonatomic) UITableViewCell *emailDevCell;
@property (strong, nonatomic) UITableViewCell *aboutCell;

@property (strong, nonatomic) UITableViewCell *shareCell;
@property (strong, nonatomic) UITableViewCell *markUsCell;

@property (strong, nonatomic) UITableViewCell *dmcaCell;

@property (strong, nonatomic) VkUser *user;



@end

@implementation SettingsViewController

-(void)loadView {
    [super loadView];
    
    self.user = [VkUser new];
    
    self.title = @"Settings";
    
    UIColor *baseColor = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:0.5f];
    
    //****| Section 0 |****\\
    
    self.signInCell = [[UITableViewCell alloc] init];
    self.signInCell.backgroundColor = baseColor;
    self.signInCell.textLabel.textColor = [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0];
    self.signInCell.textLabel.text = [self.user fullName] ? self.user.fullName : @"Войти";
    
    //****| Section 1 |****\\
    
    self.emailDevCell = [[UITableViewCell alloc] init];
    self.emailDevCell.backgroundColor = baseColor;
    self.emailDevCell.textLabel.text = @"Написать разработчику";
    
    /*
    self.aboutCell = [[UITableViewCell alloc] init];
    self.aboutCell.backgroundColor = baseColor;
    self.aboutCell.textLabel.text = @"About";
    */
    
    //****| Section 2 |****\\
    
    self.shareCell = [[UITableViewCell alloc] init];
    self.shareCell.backgroundColor = baseColor;
    self.shareCell.textLabel.text = @"Рассказать друзьям";
    
    self.markUsCell = [[UITableViewCell alloc] init];
    self.markUsCell.backgroundColor = baseColor;
    self.markUsCell.textLabel.text = @"Оценить в iTunes";
    
    //****| Section 3 |****\\
    
    self.dmcaCell = [[UITableViewCell alloc] init];
    self.dmcaCell.backgroundColor = baseColor;
    self.dmcaCell.textLabel.text = @"Report Violation";
    
    
    self.tableView.backgroundColor = [UIColor colorWithWhite:0.9f alpha:1.0f];
    self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension;
    self.tableView.scrollEnabled = NO;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tabBarController setTitle:self.tabBarItem.title];
   
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
     self.signInCell.textLabel.text = [self.user fullName] ? self.user.fullName : @"Войти";
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch(section) {
        case 0:  return 1;  // section 0 has 1 rows
        case 1:  return 1;  // section 1 has 2 row
        case 2:  return 2;  // section 2 has 2 row
        case 3:  return 1;  // section 3 has 1 row
        default: return 0;
    };
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch(indexPath.section) {
        case 0:
            switch(indexPath.row) {
                case 0: return self.signInCell;  // section 0, row 0 is the vk login
        }
        case 1:
            switch(indexPath.row) {
                case 0: return self.emailDevCell; // section 1, row 0 is the contact option
                //case 1: return self.aboutCell;    // section 1, row 1 is the about application
        }
        case 2:
            switch(indexPath.row) {
                case 0: return self.shareCell;    // section 2, row 0 is the share option
                case 1: return self.markUsCell;   // section 2, row 1 is the itunes mark
            }
        case 3:
            switch(indexPath.row) {
                case 0: return self.dmcaCell;      // section 3, row 0 is the share option
            }
    }
    return nil;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch(section) {
        case 0: return @"Войти в ВК";
        case 1: return @"Конакты";
        case 2: return @"Social";
        case 3: return @"DMCA";
    }
    return nil;
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    switch(section) {
        case 0: return @"";
        case 1: return @"";
        case 2: return @"Please shere the app in socials, and mark me on iTunes";
        case 3: return @"";
    }
    return nil;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return  [UIView new];
}


 
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0 && indexPath.row == 0) {
        
        // here is login section and row
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        
        if ([self.signInCell.textLabel.text isEqualToString:self.user.fullName]) {
            [self logOut];
        } else {
           [self signInVk];
        }
        
    } else if (indexPath.section == 1) {
        
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        // here is Information section
        
        if (indexPath.row == 0) {
            [self emailMe];
        } else {
            [self aboutApp];
        }
        
    } else if (indexPath.section == 2) {
        
        //Social section
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        
        if (indexPath.row == 0) {
            [self shareAppInSocial];
        } else {
            [self rateUsInItunes];
        }
    
    } else if(indexPath.section == 3 && indexPath.row == 0) {
        
        //report violation row
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        [self reportViolation];
    }

}

#pragma mark Methods for row andSections.


- (void) signInVk {
    [self.navigationController pushViewController:[LoginViewController new] animated:YES];
}

- (void) logOut {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:
                                 self.user.fullName message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* actionLogOut = [UIAlertAction actionWithTitle:@"Log out" style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          [[VKApi sharedInstance] forceLogout];
                                                          self.user = nil;
                                                          self.signInCell.textLabel.text = @"Sign In";
                                                      }];
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             NSLog(@"Cancel action selected");
                                                         }];
    [alert addAction:actionLogOut];
    [alert addAction:actionCancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) emailMe {
    [self mailWithSubject:@"Music Application"];
}

- (void)aboutApp {
    [self.navigationController pushViewController:[AboutViewController new] animated:YES];
}


- (void) shareAppInSocial {
    
    NSString *appUrlString = @"https://itunes.apple.com/app/id1232289938";
//#warning NEED TO ADD APP ID!!!!!!
    NSArray* sharedObjects=[NSArray arrayWithObjects:[NSString stringWithFormat:@"Слушай музыку с VK на своем iPhone оффлайн: %@", appUrlString],  nil];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:sharedObjects applicationActivities:nil];
    activityViewController.popoverPresentationController.sourceView = self.view;
    [self presentViewController:activityViewController animated:YES completion:nil];
}

- (void)rateUsInItunes {
    [[iRate sharedInstance] openRatingsPageInAppStore];
}

- (void) reportViolation {
    [self mailWithSubject:@"Violation Report"];
}


#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailWithSubject:(NSString *)subject {
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    mail.mailComposeDelegate = self;
    [mail setSubject:subject];
    [mail setToRecipients:@[@"xaolife@gmail.com"]];
    
    [self presentViewController:mail animated:YES completion:NULL];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    

    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end
