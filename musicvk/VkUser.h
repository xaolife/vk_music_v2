//
//  VkUser.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 28/04/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VkUser : NSObject

@property (strong, nonatomic, readonly) NSString *uid;
@property (strong, nonatomic, readonly) NSString *first_name;
@property (strong, nonatomic, readonly) NSString *last_name;

@property (copy, nonatomic) NSString *fullName;

- (void)deleteUserInfo;

@end
