//
//  CDMAudio+CoreDataProperties.m
//  
//
//  Created by  Dmitry Babinsky on 24/03/17.
//
//

#import "CDMAudio+CoreDataProperties.h"

@implementation CDMAudio (CoreDataProperties)

+ (NSFetchRequest<CDMAudio *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"CDMAudio"];
}

@dynamic aid;
@dynamic title;
@dynamic artist;
@dynamic duration;
@dynamic url;

@end
