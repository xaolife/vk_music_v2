//
//  SearchResultsTableController.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 27/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "SongsListTableController.h"

@interface SearchResultsTableController : UITableViewController

@property (strong, nonatomic) NSArray *searchResults;

@end
