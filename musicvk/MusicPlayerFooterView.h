//
//  MusicPlayerFooterView.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 17/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MusicPlayerFooterView : UIView

@property (strong, nonatomic) UIButton *playPauseButton;
@property (strong, nonatomic) UILabel *songTitle;
@property (strong, nonatomic) UILabel *artistName;

- (void)currentButtonState:(BOOL)state;

@end
