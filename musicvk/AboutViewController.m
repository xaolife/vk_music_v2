//
//  AboutViewController.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 27/04/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (void)loadView {
    [super loadView];
    

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithWhite:0.9f alpha:1.f];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.text = @"Приветствую!";
    [titleLabel setFont:[UIFont systemFontOfSize:30.f]];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
     [self.view addSubview:titleLabel];
    
    [titleLabel.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:self.view.frame.size.height / 12 + self.navigationController.navigationBar.frame.size.height].active = YES;
    [titleLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    
   
    
    NSString *textForTextView = @"afwfwiufhuwhifhwuehfiuhwiuiuvgwigfugifgyuqgqduyqgdgqdgyqggqwdqwidgiqwgdigqwiudqiugiqfewfgwfwegfiweiufwegfyuq";
    
    UITextView *textView = [UITextView new];
    textView.text = textForTextView;
    textView.backgroundColor = [UIColor clearColor];
    textView.editable = NO;
    [textView setFont:[UIFont systemFontOfSize:18.f]];
    textView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:textView];
    
    [textView.topAnchor constraintEqualToAnchor:titleLabel.bottomAnchor constant:self.view.frame.size.height / 12].active = YES;
    [textView.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor].active = YES;
    [textView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor constant: 8.f].active = YES;
    [textView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant: -8.f].active = YES;
    
    
}


@end
