//
//  ViewController.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 14/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "LoginViewController.h"
#import "VKApi.h"
#import "ConfirmationAuthWebView.h"
#import "SongsListTableController.h"
#import "TabBarControllerNavigation.h"

typedef void(^HandleAuthParamBlock)(NSDictionary *params);

@interface LoginViewController () <UIWebViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) UITextField *loginTextField;
@property (weak, nonatomic) UITextField *passwordTextField;
@property (strong, nonatomic) ConfirmationAuthWebView *cAuthView;
@property (strong, nonatomic) HandleAuthParamBlock authParamBlock;

@end

@implementation LoginViewController


-(void)loadView {
    [super loadView];
    [self initUserInterface];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Login VK";
    self.view.backgroundColor = [UIColor colorWithWhite:0.9f alpha:1.f];
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Log in" style:UIBarButtonItemStylePlain target:self action:@selector(handleSignInAction:)];
    self.navigationItem.rightBarButtonItem = anotherButton;
    
    
}

- (void)initUserInterface {

    UITextField *loginTextField = [[UITextField alloc] init];
    loginTextField.delegate = self;
    loginTextField.borderStyle = UITextBorderStyleRoundedRect;
    loginTextField.placeholder = @"Email";
    loginTextField.text = @"";
    loginTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    loginTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    loginTextField.keyboardType = UIKeyboardTypeEmailAddress;
    loginTextField.returnKeyType = UIReturnKeyNext;
    loginTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    loginTextField.translatesAutoresizingMaskIntoConstraints = NO;
    self.loginTextField = loginTextField;
    [self.view addSubview:loginTextField];
    
    [loginTextField.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [loginTextField.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor].active = YES;
    [loginTextField.widthAnchor constraintEqualToConstant:self.view.frame.size.width - 16.f].active = YES;
    
    UILabel *informationTextView = [[UILabel alloc] init];
    informationTextView.text = @"Данные вашей учетной записи не сохраняются в приложении";
    informationTextView.numberOfLines = 0;
    [informationTextView setTextAlignment:NSTextAlignmentCenter];
    informationTextView.textColor = [UIColor darkGrayColor];
    informationTextView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:informationTextView];
    
    [informationTextView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [informationTextView.bottomAnchor constraintEqualToAnchor:loginTextField.topAnchor constant: -16.f].active = YES;
    [informationTextView.widthAnchor constraintEqualToConstant:self.view.frame.size.width -16.f].active = YES;
    
    UITextField *passwordTextField = [[UITextField alloc] init];
    passwordTextField.delegate = self;
    passwordTextField.borderStyle = UITextBorderStyleRoundedRect;
    passwordTextField.placeholder = @"Password";
    passwordTextField.text = @"";
    passwordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    passwordTextField.keyboardType = UIKeyboardTypeDefault;
    passwordTextField.returnKeyType = UIReturnKeyDone;
    passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    passwordTextField.secureTextEntry = YES;
    passwordTextField.translatesAutoresizingMaskIntoConstraints = NO;
    self.passwordTextField = passwordTextField;
    [self.view addSubview:passwordTextField];
    
    [passwordTextField.topAnchor constraintEqualToAnchor:loginTextField.bottomAnchor constant:8.f].active = YES;
    [passwordTextField.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [passwordTextField.widthAnchor constraintEqualToConstant:self.view.frame.size.width - 16.f].active = YES;
    
}


- (IBAction)handleSignInAction:(UIButton*)sender {
    
    if ([[VKApi sharedInstance] isConnectionReach] == NO) {
        [self showAlertWithErrorMessage:@"Check internet connection"];
        return;
    }
    
    NSString *userName = [self.loginTextField text];
    NSString *password = [self.passwordTextField text];
    
    [[VKApi sharedInstance] authWithName:userName
                             andPassword:password
                                andBlock:^(AuthStates authState,
                                           NSURL *redirectURL,
                                           NSDictionary *responce) {
                                    
                                    if (authState == AUTH_SECOND_STEP_ERROR) {
                                        [self showWebViewWithUrl:redirectURL];
                                        __weak LoginViewController *weakSelf = self;
                                        self.authParamBlock = ^(NSDictionary *params){
                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"ObserveAuthSuccessResponce" object:params];
                                            [weakSelf.navigationController popViewControllerAnimated:YES];
                                            };
                                    } else if (authState == AUTH_SUCCESS) {
                                        
                                        NSLog(@"responce = %@", responce);
                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ObserveAuthSuccessResponce" object:responce];
                                        [self.navigationController popViewControllerAnimated:YES];
                                        
                                        
                                    } else if (authState == AUTH_USER_DATA_ERROR) {
                                        [self showAlertWithErrorMessage:@"Incorrect login or password"];
                                    } else if (authState == AUTH_INTERNET_CONNECTION_ERROR) {
                                        [self showAlertWithErrorMessage:@"Check internet connection"];
                                    }
                                    
                                }];
}


#pragma mark - ConfirmationAuthWebView

- (void)showWebViewWithUrl:(NSURL *)url {
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    self.cAuthView = [[ConfirmationAuthWebView alloc] initWithRequest:request];
    self.cAuthView.webView.delegate = self;
    [self.navigationController pushViewController:self.cAuthView animated:YES];
    
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSDictionary *dictionaryWithParams = [self fetchParamsFromURLString:webView.request.URL.absoluteString];
    
    if ([dictionaryWithParams objectForKey:@"access_token"]) {
        [self.navigationController popToViewController:self animated:YES];
        self.authParamBlock(dictionaryWithParams);
    }
    
}

- (NSDictionary *)fetchParamsFromURLString:(NSString *)urlString {
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSLog(@"urlString = %@", urlString);
    for (NSString *param in [urlString componentsSeparatedByString:@"&"]) {
        NSArray *paramsAndValueArray = [param componentsSeparatedByString:@"="];
        if([paramsAndValueArray count] < 2) continue;
        [params setObject:[paramsAndValueArray lastObject] forKey:[paramsAndValueArray firstObject]];
    }
    return params;
}

#pragma mark - UIAlertController

- (void)showAlertWithErrorMessage:(NSString *)message {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Auth failed"
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    if (textField == self.loginTextField) {
        [self.passwordTextField becomeFirstResponder];
    } else if (textField == self.passwordTextField) {
        [self handleSignInAction:nil];
    }
    return YES;
}



@end
