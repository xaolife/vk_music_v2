//
//  PlayerView.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 20/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PlayerViewDelegate <NSObject>

- (void)tapOnPlayButton:(UIButton *)sender;
- (void)tapOnBackwardButton:(UIButton *)sender;
- (void)tapOnForwardButton:(UIButton *)sender;
- (void)tapOnRepeatButton:(UIButton *)sender;
- (void)tapOnRandomSongButton:(UIButton *)sender;

- (void)handleChangeVolumeLevel:(UISlider *)slider;
- (void)handleChangeDurationValue:(UISlider *)slider;

@end

@interface PlayerView : UIView

- (id)initWithFrame:(CGRect)frame;
- (void)changeInterfaceWithParams:(NSDictionary *)params;

@property (weak, nonatomic) id<PlayerViewDelegate> delegate;



@end
