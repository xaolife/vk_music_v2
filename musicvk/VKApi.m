//
//  VKApi.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 14/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "VKApi.h"
#import "Auth.h"
#import "AudioRequests.h"
#import "DownloadAudio.h"
#import "CoreDataManager.h"
#import "VkUser.h"
#import "AdvertisementMob.h"
#import "Reachability.h"

@interface VKApi () {
    Auth *auth;
    AudioRequests *audio;
    DownloadAudio *audioDownloader;
    CoreDataManager *cdManager;
    AdvertisementMob *advMob;
}



@end

@implementation VKApi
@synthesize isConnectionReach;

+ (VKApi *)sharedInstance {
    static VKApi *api;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        api = [[VKApi alloc] init];
    });
    return api;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        auth = [[Auth alloc] init];
        audio = [[AudioRequests alloc] init];
        audioDownloader = [[DownloadAudio alloc] initWithDirectory];
        cdManager = [[CoreDataManager alloc] init];
        advMob = [[AdvertisementMob alloc] init];
        
        [self isConnectionReachable];
        
    }
    return self;
}

#pragma mark - Auth Mehods

- (void)authWithName:(NSString *)userName andPassword:(NSString *)password andBlock:(AuthBlock)handleResult {
    [auth authWithName:userName andPassword:password andBlock:handleResult];
}


- (BOOL)isUserAuthenticated {
    return [[VKApi sharedInstance] loadToken].length > 0 ? true : false;
}

#pragma mark - LogOut {

- (void) forceLogout {
    [auth forceLogout];
}

#pragma mark - AccessToken

- (NSString *)loadToken {
    return [auth loadToken];
}

#pragma mark - UserInfo 

- (NSString *)userFullName {
    VkUser *user = [VkUser new];
    return [user fullName];
}

#pragma mark - Audio

- (void)getUserAudio:(ComplitionParseAudio)complition {
    [audio requestUserAudioWithBlock:complition];
}

- (NSURL *) getCurrentAudioURL:(AudioModel *)song {
    
    if ([audioDownloader getAudioFromStorageWithObject:song] == nil) {
        NSURL *streamURL = [NSURL URLWithString:song.url];
        return streamURL;
    } else {
        return [audioDownloader getAudioFromStorageWithObject:song];
    }
    
}

- (NSURL *)getDownloadedAudioWithObject:(AudioModel *)song {
    return [audioDownloader getAudioFromStorageWithObject:song];
}

- (void)addSongToUserAudiosWithAudioId:(NSString *)audioId andOwnerId:(NSString *)ownerId withComplition:(void(^)(BOOL complite))complition {
    [audio requestAddAudioWithId:audioId andOwnerId:ownerId withComplition:complition];
}

#pragma mark - Download Audio

- (void) downloadAudioWithObject:(AudioModel *)song withComplition:(void(^)(BOOL complite))complition {
    [audioDownloader storageAudioWithObject:song withComplition:complition];
}

#pragma mark - CoreDataManager

- (NSArray *)arrayWithAudioModels {
    return [cdManager arrayWithAudioModelsFromCDM];
}


#pragma mark - Search

- (void)allSerchMethodsWithSerchString:(NSString *)searchStrgin andUserAudioArray:(NSArray *)userAudioArray andComplition:(void(^)(NSArray *resultArray))searchComplition {
    
    NSArray *userAuioResultArray = [self searchInUserAudioWithArray:userAudioArray andSearchString:searchStrgin];
    NSArray *downloadsResultArray = [self searchInDownloadsWithSongName:searchStrgin];
    
    [audio requestSearchAudioWithSongName:searchStrgin andBlock:^(NSArray *arrayWithAudios, NSError *error) {

        NSMutableArray *array = [[NSMutableArray alloc] init];
        [array insertObject:downloadsResultArray atIndex:0];
        [array insertObject:userAuioResultArray atIndex:1];
        [array insertObject:arrayWithAudios atIndex:2];
        
        searchComplition(array);
    }];

}

- (NSArray *)searchInUserAudioWithArray:(NSArray *)userAudio andSearchString:(NSString *)searchString {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title contains[cd] %@", searchString];
    NSArray *sortedArray = [userAudio filteredArrayUsingPredicate:predicate];
    return sortedArray;
}

- (NSArray *)searchInDownloadsWithSongName:(NSString *)searchString {
    return [cdManager searchInDownloadedSongsWithSearchString:searchString];
}

#pragma makr - Delete

- (void)deleteSongFormDownloadsWithAudio:(AudioModel *)song {
    [cdManager deleteFromCoreData:song];
    [audioDownloader deleteFileFormStorage:song];
}

#pragma mark AdvertisementMob 

- (void)showAdvWithController:(UIViewController *)vc {
    [advMob showAdWithViewController:vc];
}

- (void)isConnectionReachable {
    Reachability* reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    reach.reachableBlock = ^(Reachability*reach) {
            isConnectionReach = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"HandleReachableChange" object:[NSNumber numberWithBool:reach.isReachable]];
    };
    
    reach.unreachableBlock = ^(Reachability*reach) {
            isConnectionReach = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"HandleReachableChange" object:[NSNumber numberWithBool:reach.isReachable]];
    };
    
    [reach startNotifier];
    
    
    
}

- (void)dealloc {
    
}

@end
