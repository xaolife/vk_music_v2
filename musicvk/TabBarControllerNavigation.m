//
//  TabBarControllerNavigation.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 26/04/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "TabBarControllerNavigation.h"

@interface TabBarControllerNavigation ()

@end

@implementation TabBarControllerNavigation


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    SongsListTableController *songController = [[SongsListTableController alloc] init];
    DownloadsViewController *downloadsController = [[DownloadsViewController alloc] init];
    SettingsViewController *settingsController = [[SettingsViewController alloc] init];
    
    NSMutableArray *tabViewControllers = [[NSMutableArray alloc] init];
    [tabViewControllers addObject:songController];
    [tabViewControllers addObject:downloadsController];
    [tabViewControllers addObject:settingsController];
    
     
    [self setViewControllers:tabViewControllers];
    
    //UIColor *tabItemsSelectedColor = [UIColor colorWithRed:(192 / 255.f) green:(57 / 255.f) blue:(43 / 255.f) alpha:1.f]; //rgb(192,57,43)
    UIColor *tabItemsNormalColor = [UIColor colorWithRed:(147 / 255.f) green:(147 / 255.f) blue:(147 / 255.f) alpha:1.f]; // (147, 147, 147)
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : tabItemsNormalColor }
                                             forState:UIControlStateNormal];
    
    songController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"My Audios" image:[UIImage imageNamed:@"audioOnline50.png"] tag:0];
    downloadsController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Downloads" image:[UIImage imageNamed:@"downloads50.png"] tag:1];
    settingsController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Settings" image:[UIImage imageNamed:@"settings50.png"] tag:2];

}

@end
