//
//  DownloadAudio.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 20/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "DownloadAudio.h"
#import <AFNetworking.h>
#import "CoreDataManager.h"

@interface DownloadAudio ()

@property (strong, nonatomic) NSString *dataPath;

@end

@implementation DownloadAudio
@synthesize dataPath;

- (id)initWithDirectory {
    self = [super init];
    if (self) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        dataPath = [documentsDirectory stringByAppendingPathComponent:@"VkMusicApplication"];
        
        NSError *error = nil;
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
            [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    return self;
}

- (void)storageAudioWithObject:(AudioModel *)audio withComplition:(void(^)(BOOL complite))complition {
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:audio.url]];
    AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
    
    NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        //progress
    } destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        
        NSURL *documentsDirectoryURL = [NSURL fileURLWithPath:dataPath];
        return [documentsDirectoryURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@-%@.mp3", audio.artist, audio.title]];
        
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {

        if (error != nil) {
            complition(NO);
            return ;
        }
        
        dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            CoreDataManager *cdManager = [[CoreDataManager alloc] init];
            [cdManager storageSongWith:audio andNewfilePath:filePath.absoluteString];
            complition(YES);
        });

    }];
    
    [downloadTask resume];
    
}

- (NSURL *)getAudioFromStorageWithObject:(AudioModel *)audio {
    
    NSString *fileName = [NSString stringWithFormat:@"/%@-%@.mp3", audio.artist, audio.title];
    NSString *tempPath = [dataPath stringByAppendingString:fileName];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:tempPath]) {
        NSURL *url = [NSURL fileURLWithPath:tempPath];
        //NSLog(@"ur; = %@", url);
        return url;
    }
    
    return nil;
}

- (void)deleteFileFormStorage:(AudioModel *)audio {
    
    NSString *fileName = [NSString stringWithFormat:@"/%@-%@.mp3", audio.artist, audio.title];
    NSString *tempPath = [dataPath stringByAppendingString:fileName];
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:tempPath error:&error];

    if (error != nil) {
        NSLog(@"error in downloader = %@", [error localizedDescription]);
    }
    
}


@end
