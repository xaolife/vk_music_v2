//
//  VKAccessToken.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 14/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "VKAccessToken.h"

@implementation VKAccessToken
static NSString * ACCESS_TOKEN_DEFAULT_KEY = @"ACCESS_TOKEN_DEFAULT_KEY";
static NSString * USER_ID_DEFAULT_KEY = @"USER_ID_DEFAULT_KEY";


- (id)init {
    self = [super init];
    if (self) {
        _secret = @"AlVXZFMUqyrnABp8ncuU";
        _clientId = @"3697615";
    }
    return self;
}

- (void)saveAuthInfo:(VKAccessToken *)token {
    //NSLog(@"token = %@", token.accessToken);
    [[NSUserDefaults standardUserDefaults] setObject:token.accessToken forKey:ACCESS_TOKEN_DEFAULT_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:token.userId forKey:USER_ID_DEFAULT_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)loadAccessToken {
    self.accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESS_TOKEN_DEFAULT_KEY];
    return self.accessToken;
}

- (NSString *)loadUserId {
    self.userId = [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID_DEFAULT_KEY];
    return self.userId;
}

- (void)deleteAuthInfo {
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:ACCESS_TOKEN_DEFAULT_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:USER_ID_DEFAULT_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
