//
//  AudioCell.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 16/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AudioCell;

@protocol AudioCellDelegate <NSObject>

- (void)downloadButtonTapped:(UIButton *)sender inCell:(AudioCell *)cell andActivityIndicator:(UIActivityIndicatorView *)activityIndicator;

@optional

- (void)addAudioButtonTapped:(UIButton *)sender inCell:(AudioCell *)cell;

@end

@interface AudioCell : UITableViewCell

@property (strong, nonatomic) UIButton *downloadButton;
@property (strong, nonatomic) UIButton *addButton;

@property (strong, nonatomic) UILabel *title;
@property (strong, nonatomic) UILabel *artist;
@property (strong, nonatomic) UILabel *songDurationLabel;

@property (strong, nonatomic) UIImageView *playImageView;

@property (weak, nonatomic) id<AudioCellDelegate> delegate;






//- (void)initUIcomponentsWithReuseIdentifier;

@end
