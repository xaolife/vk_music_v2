//
//  VkUser.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 28/04/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "VkUser.h"

@implementation VkUser
@synthesize fullName = _fullName;



- (NSString *)fullName {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"userFullName"];
}

- (void)setFullName:(NSString *)fullName {
    _fullName = [fullName copy];
    [[NSUserDefaults standardUserDefaults] setObject:fullName forKey:@"userFullName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void)deleteUserInfo {
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"userFullName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
