//
//  GetUserInfo.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 27/04/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "GetUserInfo.h"
#import <AFNetworking.h>
#import "VKAccessToken.h"
#import "VkUser.h"

@interface GetUserInfo ()

@property (strong, nonatomic) NSString *userId;

@end

@implementation GetUserInfo

- (instancetype)init {
    self = [super init];
    if (self) {
        VKAccessToken *token = [[VKAccessToken alloc] init];
        self.userId = [token loadUserId];
    }
    return self;
}

- (void)requstUserInfo:(void(^)(VkUser *user))complition {
    
    NSString *url = @"https://api.vk.com/method/users.get";
    NSDictionary *parametrs = @{@"user_ids"  : self.userId,
                                @"name_case" : @"nom"
                                };
    
    [[AFHTTPSessionManager manager] GET:url parameters:parametrs progress:nil success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary *responseObject) {
        
        
        NSDictionary *responce = [[responseObject valueForKey:@"response"] objectAtIndex:0];
        VkUser *user = [VkUser new];
        [user setValuesForKeysWithDictionary:responce];
        
        complition(user);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];

}


@end
