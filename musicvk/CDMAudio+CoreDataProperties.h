//
//  CDMAudio+CoreDataProperties.h
//  
//
//  Created by  Dmitry Babinsky on 24/03/17.
//
//

#import "CDMAudio+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CDMAudio (CoreDataProperties)

+ (NSFetchRequest<CDMAudio *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *aid;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *artist;
@property (nullable, nonatomic, copy) NSNumber *duration;
@property (nullable, nonatomic, copy) NSString *url;

@end

NS_ASSUME_NONNULL_END
