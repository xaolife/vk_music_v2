//
//  DownloadsViewController.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 23/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "DownloadsViewController.h"
#import "LoginViewController.h"
#import "VKApi.h"
#import "AudioCell.h"
#import "AudioModel.h"
#import "PlayerViewController.h"

@interface DownloadsViewController ()

@property (strong, nonatomic) NSMutableArray *arrayWithDownloadAudios;
@property (strong, nonatomic) PlayerViewController *playerViewController;

@end

@implementation DownloadsViewController
@synthesize playerViewController;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.contentInset = UIEdgeInsetsMake(0., 0., CGRectGetHeight(self.tabBarController.tabBar.frame), 0);
    playerViewController = [PlayerViewController sharedPlayer];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tabBarController setTitle:self.tabBarItem.title];
    
    if ([[VKApi sharedInstance] isUserAuthenticated] == YES) {
        self.arrayWithDownloadAudios = (NSMutableArray *)[[VKApi sharedInstance] arrayWithAudioModels];
        
        if (self.arrayWithDownloadAudios.count > 0) {
            self.tableView.backgroundView = [UIView new];
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            [self.tableView reloadData];
        } else {
            UILabel *emptyTable = [UILabel new];
            emptyTable.text = @"Список пуст, загрузите аудиозаписи.";
            [emptyTable setFont:[UIFont systemFontOfSize:18.f]];
            [emptyTable setTextColor:[UIColor darkGrayColor]];
            emptyTable.numberOfLines = 0;
            [emptyTable setTextAlignment:NSTextAlignmentCenter];
            self.tableView.backgroundView = emptyTable;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            [self.tableView reloadData];
        }
    } else {
        UIButton *signInButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [signInButton.titleLabel setFont:[UIFont boldSystemFontOfSize:18.f]];
        [signInButton setTitle:@"Войти" forState:UIControlStateNormal];
        [signInButton addTarget:self action:@selector(signInAction:) forControlEvents:UIControlEventTouchUpInside];
        [signInButton setBackgroundColor:[UIColor colorWithWhite:0.9f alpha:1.f]];
        
        self.tableView.backgroundView = signInButton;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
}

- (void)signInAction:(UIButton *)sender {
    [self.navigationController pushViewController:[LoginViewController new] animated:YES];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([[VKApi sharedInstance] isUserAuthenticated] == YES) {
        [[VKApi sharedInstance] showAdvWithController:self];
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arrayWithDownloadAudios count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellId = @"downloadsIdentifier";
    
    AudioCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (!cell) {
        cell = [[AudioCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellId];
        
    }

    AudioModel *audioModel = self.arrayWithDownloadAudios[indexPath.row];
    cell.title.text = audioModel.title;
    cell.artist.text = audioModel.artist;
    cell.songDurationLabel.text = [NSString convertSecondsStringToCorrectFormat:audioModel.duration];
    
    
    if ([playerViewController isSame:[[VKApi sharedInstance] getCurrentAudioURL:audioModel]]) {
        [cell.playImageView setImage:[UIImage imageNamed:@"pauseFilled50icon"]];
    } else {
        [cell.playImageView setImage:[UIImage imageNamed:@"playFilled50Icon"]];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    playerViewController.song = self.arrayWithDownloadAudios[indexPath.row];
    playerViewController.arrayWithAudio = self.arrayWithDownloadAudios;
    playerViewController.index = indexPath.row;
    [self.navigationController pushViewController:playerViewController animated:YES];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

    if (editingStyle == UITableViewCellEditingStyleDelete) {

        AudioModel *audioForDelete = self.arrayWithDownloadAudios[indexPath.row];
        NSURL *currentAudioUrl = [[VKApi sharedInstance] getCurrentAudioURL:audioForDelete];
        
        if ([playerViewController isSame:currentAudioUrl] == YES) {
            [self showAllertForDelete];
            return;
        } else {
            [self.arrayWithDownloadAudios removeObjectAtIndex:indexPath.row];
            [[VKApi sharedInstance] deleteSongFormDownloadsWithAudio:audioForDelete];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
        
    }
}

- (void)showAllertForDelete {
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:@"This audio is playing now, can't delete it"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}



@end
