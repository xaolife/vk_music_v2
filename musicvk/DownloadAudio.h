//
//  DownloadAudio.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 20/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AudioModel.h"

@interface DownloadAudio : NSObject

- (id)initWithDirectory;
- (void)storageAudioWithObject:(AudioModel *)audio withComplition:(void(^)(BOOL complite))complition;
- (NSURL *)getAudioFromStorageWithObject:(AudioModel *)audio;
- (void)deleteFileFormStorage:(AudioModel *)audio;

@end
