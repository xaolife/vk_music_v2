//
//  Auth.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 15/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "Auth.h"
#import <AFNetworking.h>
#import "VKAccessToken.h"
#import "VkUser.h"
#import "GetUserInfo.h"


@interface Auth ()

@property (strong, nonatomic) VKAccessToken *accessToken;
@property (strong, nonatomic) VkUser *user;

@end

@implementation Auth


- (instancetype)init {
    self = [super init];
    if (self) {
        self.accessToken = [[VKAccessToken alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeAuthSuccessResponce:) name:@"ObserveAuthSuccessResponce" object:nil];
        self.user = [[VkUser alloc] init];
    }
    return self;
}


- (void)authWithName:(NSString *)userName andPassword:(NSString *)password andBlock:(AuthBlock)handleResult {
    
    NSString *url = @"https://oauth.vk.com/token?";
    
    NSDictionary *parametrs = @{@"grant_type"    : @"password",
                                @"client_id"     : self.accessToken.clientId,
                                @"client_secret" : self.accessToken.secret,
                                @"username"      : userName,
                                @"password"      : password };
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:parametrs
        progress:nil
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

             handleResult(AUTH_SUCCESS, nil, responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             
             if (error.code == -1009 || error.code == -1001) {
                 handleResult(AUTH_INTERNET_CONNECTION_ERROR, nil, nil);
                 return ;
             } else {
                 NSString* errorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
                 NSString *url = [self stringBetweenString:@"\"redirect_uri\":\"" andString:@"\"}" innerString:errorResponse];
                 NSString *clearString = [url stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                 NSURL *redirectURL = [NSURL URLWithString:clearString];
                 
                 if (!url && error.code == -1011) {
                     handleResult(AUTH_USER_DATA_ERROR, nil, nil);
                     return ;
                 }
                 
                 dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     handleResult(AUTH_SECOND_STEP_ERROR, redirectURL, nil);
                 });
             }
         }];
}

#pragma mark - Logout 

- (void) forceLogout {
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
    
    for (NSHTTPCookie *cookie in cookies)
        if (NSNotFound != [cookie.domain rangeOfString:@"vk.com"].location) {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage]
             deleteCookie:cookie];
        }
    
    [self.accessToken deleteAuthInfo];
    [self.user deleteUserInfo];
    
    self.accessToken = nil;
    self.user = nil;
}


#pragma mark - Methods


- (NSString*)stringBetweenString:(NSString*)start andString:(NSString*)end innerString:(NSString*)str {
    
    NSScanner* scanner = [NSScanner scannerWithString:str];
    [scanner setCharactersToBeSkipped:nil];
    [scanner scanUpToString:start intoString:NULL];
    if ([scanner scanString:start intoString:NULL]) {
        NSString* result = nil;
        if ([scanner scanUpToString:end intoString:&result]) {
            return result;
        }
    }
    return nil;
}

#pragma mark - AccessToken

- (void)saveAccessToken:(NSString *)accessToken andUserId:(NSString *)userId {
    
    [self.accessToken setAccessToken:accessToken];
    [self.accessToken setUserId:userId];
    [self.accessToken saveAuthInfo:self.accessToken];
    
}

- (NSString *)loadToken {
    return [self.accessToken loadAccessToken];
}

#pragma mark - User 

- (void)saveUserFullName:(NSString *)name {
    [self.user setFullName:name];
}

#pragma mark - Notifications

- (void)observeAuthSuccessResponce:(NSNotification *)notification {
    [self saveAccessToken:[notification.object objectForKey:@"access_token"] andUserId:[notification.object objectForKey:@"user_id"]];
    GetUserInfo *userInfe = [GetUserInfo new];
    [userInfe requstUserInfo:^(VkUser *user) {
        NSString *name = [NSString stringWithFormat:@"%@ %@", user.first_name, user.last_name];
        [self saveUserFullName:name];
    }];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
