//
//  AudioRequests.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 16/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "AudioRequests.h"
#import "VKAccessToken.h"
#import <AFNetworking.h>
#import "AudioModel.h"

typedef void (^ ComplitionBlock)(NSDictionary *responce, NSError *error);

@interface AudioRequests ()

@property (strong, nonatomic) VKAccessToken *accessToken;
@property (copy, nonatomic) ComplitionBlock complitionBlock;

@end

@implementation AudioRequests

- (instancetype)init {
    self = [super init];
    if (self) {
        self.accessToken = [[VKAccessToken alloc] init];
        
        
    }
    return self;
}

- (void)requestUserAudioWithBlock:(ComplitionParseAudio)complition {
    NSString *url = @"https://api.vk.com/method/audio.get";
    NSDictionary *parametrs = @{@"owner_id"     : [self.accessToken loadUserId],
                                @"access_token" : [self.accessToken loadAccessToken],
                                };

     __block NSMutableArray *arrayWithAudios = [NSMutableArray new];
    
    [[AFHTTPSessionManager manager] GET:url parameters:parametrs progress:nil success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary *responseObject) {
        
        NSArray *array = [responseObject valueForKey:@"response"];
        
        [self parseJSONWithArray:array andAudioModelsArray:arrayWithAudios];
        
        complition (arrayWithAudios, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complition (nil, error);
    }];
}


- (void)requestSearchAudioWithSongName:(NSString *)songName andBlock:(ComplitionParseAudio)complition {
    
    NSString *url = @"https://api.vk.com/method/audio.search";
    
    NSDictionary *parametrs = @{@"q"                : songName,
                                @"access_token"     : [self.accessToken loadAccessToken],
                                @"count"            : @"50" };
    
    __block NSMutableArray *arrayWithSearchAudios = [NSMutableArray new];
    
    [[AFHTTPSessionManager manager] GET:url parameters:parametrs progress:nil success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary *responseObject) {
        
        NSArray *array = [responseObject valueForKey:@"response"];
        [self parseJSONWithArray:array andAudioModelsArray:arrayWithSearchAudios];
        
        complition(arrayWithSearchAudios, nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"get audio error = %@", [error localizedDescription]);
        complition(nil, error);
    }];
}

- (void)requestAddAudioWithId:(NSString *)audioId andOwnerId:(NSString *)ownerId withComplition:(void(^)(BOOL complite))complition {
    
    NSString *url = @"https://api.vk.com/method/audio.add";
    
    NSDictionary *parametrs = @{@"aid"     : audioId,
                                @"oid"     : ownerId,
                                @"access_token" : [self.accessToken loadAccessToken]};
    
    
    [[AFHTTPSessionManager manager] GET:url parameters:parametrs progress:nil success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary *responseObject) {
        
        complition(YES);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"get audio error = %@", [error localizedDescription]);
        complition(NO);
    }];
}

#pragma mark - parse method 

- (void)parseJSONWithArray:(NSArray *)responceArray andAudioModelsArray:(NSMutableArray *)arrayWithResult {
    
    for (id dic in responceArray) {
        if ([dic isKindOfClass:[NSDictionary class]]) {
            AudioModel *audioModel = [[AudioModel alloc] init];
            audioModel.isPlaySelected = NO;
            [audioModel setValuesForKeysWithDictionary:dic];
            [arrayWithResult addObject:audioModel];
        }
    }
}

@end
