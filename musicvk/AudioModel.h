//
//  AudioModel.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 16/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AudioModel : NSObject

@property (strong, nonatomic) NSString *artist;
@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *duration;
@property (strong, nonatomic) NSString *aid;
@property (strong, nonatomic) NSString *owner_id;
@property (strong, nonatomic) NSString *lyrics_id;
@property (strong, nonatomic) NSString *genre;
@property (strong, nonatomic) NSString *content_restricted;
@property (strong, nonatomic) NSString *no_search;
@property (strong, nonatomic) NSString *album;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *url;


@property (strong, nonatomic) NSString *suggestedFilename;

@property (assign, nonatomic) BOOL isPlaySelected;

@end
