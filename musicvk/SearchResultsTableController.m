//
//  SearchResultsTableController.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 27/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "SearchResultsTableController.h"
#import "PlayerViewController.h"
#import "VKApi.h"

@interface SearchResultsTableController () <AudioCellDelegate>

@end

@implementation SearchResultsTableController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.contentInset = UIEdgeInsetsMake(0., 0., CGRectGetHeight(self.tabBarController.tabBar.frame), 0);
}

- (void)showAlertInternetConnnectionError {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Connection error"
                                                                   message:@"Check your internetConnection"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return [self.searchResults[0] count] ? @"Downloads Audio" : nil;
    } else if (section == 1) {
        return [self.searchResults[1] count] ? @"User Audio"      : nil;
    } else if (section == 2) {
        return [self.searchResults[2] count] ? @"Global Audio"    : nil;
    }
    return nil;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.searchResults.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *tempArray = [self.searchResults objectAtIndex:section] ;
    return tempArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellId = @"songListIdentifier";
    NSString *key = @"";
    
    switch (indexPath.section) {
        case 0:
            key = @"Downloads Audio";
            break;
        case 1:
            key = @"User Audio";
            break;
        case 2:
            key = @"Global Audio";
            cellId = @"searchCellIdentifier";
            break;
        default:
            NSLog(@"Unknown section.");
            break;
    }
    
    AudioCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (!cell) {
        cell = [[AudioCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellId];
        
    }
    
    NSArray *currentArrayWithAudio = [self.searchResults objectAtIndex:indexPath.section];
    
    cell.delegate = self;
    
    AudioModel *audio = currentArrayWithAudio[indexPath.row];
    
    [self configureCell:cell withAudio:audio];
    
    return cell;
}

- (void)configureCell:(AudioCell *)cell withAudio:(AudioModel *)audio {
    
    cell.title.text = audio.title;
    cell.artist.text = audio.artist;
    cell.songDurationLabel.text = [NSString convertSecondsStringToCorrectFormat:audio.duration];
    
    if ([[VKApi sharedInstance] getDownloadedAudioWithObject:audio] == nil ) {
        [cell.downloadButton setHidden:NO];
    } else {
        [cell.downloadButton setHidden:YES];
    }
    
}
- (void)downloadButtonTapped:(UIButton *)sender inCell:(AudioCell *)cell andActivityIndicator:(UIActivityIndicatorView *)activityIndicator {
    
    [sender setHidden:YES];
    [activityIndicator startAnimating];
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSArray *tempArray = self.searchResults[indexPath.section];
    AudioModel *song = tempArray[indexPath.row];
    [[VKApi sharedInstance] downloadAudioWithObject:song withComplition:^(BOOL complite) {
        if (complite == YES) {
            [activityIndicator stopAnimating];
        } else {
            [sender setHidden:NO];
            [self showAlertInternetConnnectionError];
        }
    }];
}

- (void)addAudioButtonTapped:(UIButton *)sender inCell:(AudioCell *)cell {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSArray *tempArray = self.searchResults[indexPath.section];
    AudioModel *song = tempArray[indexPath.row];
    [[VKApi sharedInstance] addSongToUserAudiosWithAudioId:song.aid andOwnerId:song.owner_id withComplition:^(BOOL complite) {
        if (complite == YES) {
            [sender setHidden:YES];
        } else {
            [sender setHidden:NO];
            [self showAlertInternetConnnectionError];
        }
    }];
}

@end
