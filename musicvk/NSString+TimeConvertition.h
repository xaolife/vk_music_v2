//
//  NSString+TimeConvertition.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 24/04/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (TimeConvertition)
+ (NSString *)convertSecondsStringToCorrectFormat:(NSString *)duration;
@end
