//
//  AudioRequests.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 16/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EnumAndBlockHeader.h"

@interface AudioRequests : NSObject


- (void)requestUserAudioWithBlock:(ComplitionParseAudio)complition;

- (void)requestSearchAudioWithSongName:(NSString *)songName andBlock:(ComplitionParseAudio)complition;

- (void)requestAddAudioWithId:(NSString *)audioId andOwnerId:(NSString *)ownerId withComplition:(void(^)(BOOL complite))complition;


@end
