//
//  Auth.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 15/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EnumAndBlockHeader.h"

@interface Auth : NSObject

@property (assign, nonatomic) AuthStates authState;

- (void)authWithName:(NSString *)userName andPassword:(NSString *)password andBlock:(AuthBlock)handleResult;

- (NSString *)loadToken;
- (void) forceLogout;

@end
