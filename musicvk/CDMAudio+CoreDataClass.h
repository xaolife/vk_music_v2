//
//  CDMAudio+CoreDataClass.h
//  
//
//  Created by  Dmitry Babinsky on 24/03/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CDMAudio : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "CDMAudio+CoreDataProperties.h"
