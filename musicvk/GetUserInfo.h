//
//  GetUserInfo.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 27/04/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VkUser.h"

@interface GetUserInfo : NSObject

- (void)requstUserInfo:(void(^)(VkUser *user))complition;

//- (NSString *)userFullName;

@end
