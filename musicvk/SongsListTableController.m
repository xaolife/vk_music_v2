//
//  SongsListTableController.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 15/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "SongsListTableController.h"
#import "VKApi.h"
#import "LoginViewController.h"
#import "MusicPlayerFooterView.h"
#import "PlayerViewController.h"
#import "DownloadsViewController.h"
#import "SearchResultsTableController.h"


@interface SongsListTableController () <UITableViewDataSource, UITableViewDelegate, AudioCellDelegate, UISearchResultsUpdating, UISearchBarDelegate>

@property (strong, nonatomic) NSArray *arrayWithAudios;
@property (strong, nonatomic) NSString *cellId;
@property (strong, nonatomic) MusicPlayerFooterView *footerView;

@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) SearchResultsTableController *resultsTableController;

@property (strong, nonatomic) PlayerViewController *playerViewController;

@property BOOL searchControllerWasActive;
@property BOOL searchControllerSearchFieldWasFirstResponder;

@property BOOL isConnectionReach;

@end

@implementation SongsListTableController
@synthesize cellId;
@synthesize footerView;
@synthesize playerViewController;

- (id)init {
    self = [super init];
    
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleReachableChange:) name:@"HandleReachableChange" object:nil];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *downloadBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"download50icon"] style:UIBarButtonItemStylePlain target:self action:@selector(pushToDownloadsController:)];
    self.navigationItem.leftBarButtonItem = downloadBarButton;
    
    cellId = @"songListIdentifier";
    
    [self.tableView registerClass:[AudioCell class] forCellReuseIdentifier:cellId];
    self.tableView.delegate = self;
    self.tableView.contentInset = UIEdgeInsetsMake(0., 0., CGRectGetHeight(self.tabBarController.tabBar.frame), 0);
    
    [self setupSearchController];
    
    playerViewController = [PlayerViewController sharedPlayer];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tabBarController setTitle:self.tabBarItem.title];
    
    if ([[VKApi sharedInstance] isUserAuthenticated] == YES) {
        [self refreshObjectsInAudioArray];
        
        self.searchController.searchBar.hidden = NO;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeArrayWithAudios:) name:@"ObserveArrayWithAudios" object:nil];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        [self.tableView reloadData];
    } else {
        self.searchController.searchBar.hidden = YES;
        UIButton *signInButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [signInButton.titleLabel setFont:[UIFont boldSystemFontOfSize:18.f]];
        [signInButton setTitle:@"Войти" forState:UIControlStateNormal];
        [signInButton addTarget:self action:@selector(signInAction:) forControlEvents:UIControlEventTouchUpInside];
        [signInButton setBackgroundColor:[UIColor colorWithWhite:0.9f alpha:1.f]];
        
        self.tableView.backgroundView = signInButton;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
}

- (void)signInAction:(UIButton *)sender {
    [self.navigationController pushViewController:[LoginViewController new] animated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([[VKApi sharedInstance] isUserAuthenticated] == YES) {
        [[VKApi sharedInstance] showAdvWithController:self];
    }
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ObserveArrayWithAudios" object:nil];
}

 
- (void)refreshObjectsInAudioArray {
    
    //if (self.isConnectionReach == NO) {
    //    [self showAlertInternetConnnectionError];
    //    return;
    //}
    
    __weak SongsListTableController *weakSelf = self;
    [[VKApi sharedInstance] getUserAudio:^(NSArray *arrayWithAudios, NSError *error) {
        if (error) {
            [self showAlertInternetConnnectionError];
        } else {
            weakSelf.arrayWithAudios = [NSArray arrayWithArray:arrayWithAudios];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ObserveArrayWithAudios" object:nil];
        }
    }];
}


- (void)showAlertInternetConnnectionError {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Connection error"  message:@"Check your internet connection"
                                                        preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)setupSearchController {
    
    self.resultsTableController = [[SearchResultsTableController alloc] init];
    
    _searchController = [[UISearchController alloc] initWithSearchResultsController:self.resultsTableController];
    self.searchController.searchResultsUpdater = self;
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    self.resultsTableController.tableView.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    
    [self.searchController.searchBar setBackgroundImage:[UIImage new]];
    [self.searchController.searchBar setBarTintColor:[UIColor colorWithWhite:0.97f alpha:1.f]];
    [self.searchController.searchBar setBackgroundColor:[UIColor colorWithWhite:0.97f alpha:1.f]];
    //[self.searchController.searchBar setTintColor:[UIColor colorWithRed:(192 / 255.f) green:(57 / 255.f) blue:(43 / 255.f) alpha:1.f]];
    
    UITextField *searchField = [self.searchController.searchBar valueForKey:@"searchField"];
    searchField.backgroundColor = [UIColor colorWithWhite:0.92f alpha:1.f];
}

#pragma mark - DownloadsViewController

- (void)pushToDownloadsController:(UIBarButtonItem *)sender {
    DownloadsViewController *downloadViewController = [[DownloadsViewController alloc] init];
    [self.navigationController pushViewController:downloadViewController animated:YES];
}

#pragma mark - Notification

- (void)observeArrayWithAudios:(NSNotification *)notification {
    [self.tableView reloadData];
}

- (void)handleReachableChange:(NSNotification *)notification {
    self.isConnectionReach = [notification.object boolValue];
    if ([[VKApi sharedInstance] isUserAuthenticated] == YES && self.isConnectionReach == YES) {
        [self refreshObjectsInAudioArray];
    } else if (self.isConnectionReach == NO) {
        [self showAlertInternetConnnectionError];
    }

}

#pragma mark - UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arrayWithAudios count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cellId = @"songListIdentifier";
    
    AudioCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (!cell) {
        cell = [[AudioCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellId];
        
    }
    
    cell.delegate = self;
    
    AudioModel *audio = self.arrayWithAudios[indexPath.row];
    [self configureCell:cell withAudio:audio];
    
    return cell;
}

- (void)configureCell:(AudioCell *)cell withAudio:(AudioModel *)audio {
    
    cell.title.text = audio.title;
    cell.artist.text = audio.artist;
    cell.songDurationLabel.text = [NSString convertSecondsStringToCorrectFormat:audio.duration];
    
    NSURL *currentAudioUrl = [[VKApi sharedInstance] getDownloadedAudioWithObject:audio];
    
    if (currentAudioUrl == nil ) {
        [cell.downloadButton setHidden:NO];
    } else {
        [cell.downloadButton setHidden:YES];
    }
    
    if ([playerViewController isSame:[NSURL URLWithString:audio.url]] || [playerViewController isSame:currentAudioUrl]) {
        [cell.playImageView setImage:[UIImage imageNamed:@"pauseFilled50icon"]];
    } else {
        [cell.playImageView setImage:[UIImage imageNamed:@"playFilled50Icon"]];
    }
    
}


#pragma makr - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AudioModel *currentSong = (tableView == self.tableView) ? self.arrayWithAudios[indexPath.row] : [[self.resultsTableController.searchResults objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    NSArray *currentAudioArray = (tableView == self.tableView) ? self.arrayWithAudios : [self.resultsTableController.searchResults objectAtIndex:indexPath.section];
    
    playerViewController.song = currentSong;
    playerViewController.arrayWithAudio = currentAudioArray;
    playerViewController.index = indexPath.row;
    [self.navigationController pushViewController:playerViewController animated:YES];
}

#pragma mark - AudioCellDelegate

- (void)downloadButtonTapped:(UIButton *)sender inCell:(AudioCell *)cell andActivityIndicator:(UIActivityIndicatorView *)activityIndicator {
    
    if (self.isConnectionReach == NO) {
        [self showAlertInternetConnnectionError];
        return ;
    }
    
    [sender setHidden:YES];
    [activityIndicator startAnimating];
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    AudioModel *song = self.arrayWithAudios[indexPath.row];
    [[VKApi sharedInstance] downloadAudioWithObject:song withComplition:^(BOOL complite) {
        if (complite == YES) {
            [activityIndicator stopAnimating];
        } else {
            [sender setHidden:NO];
            [self showAlertInternetConnnectionError];
        }
    }];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self refreshObjectsInAudioArray];
}

#pragma mark - UISearchResultUpdater 

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    __weak SongsListTableController *weakSelf = self;
    
    if (searchController.searchBar.text.length > 0) {
    
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[VKApi sharedInstance] allSerchMethodsWithSerchString:searchController.searchBar.text andUserAudioArray:weakSelf.arrayWithAudios andComplition:^(NSArray *resultArray) {
                
                SearchResultsTableController *tableController = (SearchResultsTableController *)self.searchController.searchResultsController;
                tableController.searchResults = resultArray;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [tableController.tableView reloadData];
                });
            }];
        });
    }
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"HandleReachableChange" object:nil];
}

@end
