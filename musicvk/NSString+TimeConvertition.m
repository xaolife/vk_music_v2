//
//  NSString+TimeConvertition.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 24/04/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "NSString+TimeConvertition.h"

@implementation NSString (TimeConvertition)

+ (NSString *)convertSecondsStringToCorrectFormat:(NSString *)duration {
    
    NSNumber *durationInSeconds = [NSNumber numberWithInteger:duration.intValue];
    int minutes = durationInSeconds.integerValue/60.f;
    int seconds = fmodf(durationInSeconds.integerValue,60.f);
    NSString *minString = minutes < 10 ? [NSString stringWithFormat:@"0%d", minutes] : [NSString stringWithFormat:@"%d", minutes];
    NSString *secString = seconds < 10 ? [NSString stringWithFormat:@"0%d", seconds] : [NSString stringWithFormat:@"%d", seconds];
    
    return [NSString stringWithFormat:@"%@:%@", minString, secString];
}

@end
