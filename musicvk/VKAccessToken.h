//
//  VKAccessToken.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 14/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VKAccessToken : NSObject

@property(nonatomic, readwrite, copy) NSString *accessToken;
@property(nonatomic, readwrite, copy) NSString *userId;
@property(nonatomic, readonly, copy) NSString *secret;
@property(nonatomic, readonly, copy) NSString *clientId;

/*
+ (void)save:(NSString *)service data:(VKAccessToken *)data;
+ (VKAccessToken *)load:(NSString *)service;
+ (void)delete:(NSString *)service;
*/

- (void)saveAuthInfo:(VKAccessToken *)token;

- (NSString *)loadAccessToken;
- (NSString *)loadUserId;
- (void)deleteAuthInfo;

@end
