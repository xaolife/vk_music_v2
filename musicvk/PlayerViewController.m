//
//  PlayerViewController.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 17/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "PlayerViewController.h"
#import "PlayerView.h"
#import "AudioModel.h"
//#import "PlayerManager.h"
#import "VKApi.h"
#import <MediaPlayer/MediaPlayer.h>

@import AVFoundation;

@interface PlayerViewController () <PlayerViewDelegate>

@property (strong, nonatomic) PlayerView *playerView;
@property (strong, nonatomic) id timeObserverToken;
@property (weak, nonatomic)   UIButton *playButton;
@property (weak, nonatomic) UIImage *currentArtworkImage;
@property (assign, nonatomic) NSInteger randomIndex;


//refactoring

@property (strong, nonatomic) AVPlayer *audioPlayer;
@property (assign, nonatomic) float volume;
           
@end

@implementation PlayerViewController

#pragma mark - Singleton

+ (PlayerViewController *)sharedPlayer {
    static PlayerViewController *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[PlayerViewController alloc] init];
    });
    return manager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.audioPlayer = [[AVPlayer alloc] init];
        self.volume = [[AVAudioSession sharedInstance] outputVolume];
        
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(handleDoneAction:)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(handleDoneAction:)];
    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor darkGrayColor]];
    self.navigationController.navigationBar.translucent = NO;
    
    CGRect currentFrame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), self.view.frame.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height);
    
    self.playerView = [[PlayerView alloc] initWithFrame:currentFrame];
    self.playerView.delegate = self;
    [self.view addSubview:self.playerView];
    

    [self addRemoteControls];
    
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    [self checkNowPlaying];
    NSDictionary *params = [NSDictionary dictionaryWithDictionary:[self configureParamDictionary]];
    [self.playerView changeInterfaceWithParams:params];
    
    [self startWithTimeObserver];
    
    [[AVAudioSession sharedInstance] addObserver:self forKeyPath:@"outputVolume" options:0 context:nil];
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    __weak PlayerViewController *weakSelf = self;
    
    [self getSongImage:[[VKApi sharedInstance] getCurrentAudioURL:self.song] withComplition:^(UIImage *image) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"HandleArtworkUpdate" object:image];
        weakSelf.currentArtworkImage = image;
    }];
    
    [self addNowPlayingInfo:self.song.artist songTitle:self.song.title playbackDuratipn:[NSNumber numberWithDouble:self.song.duration.doubleValue] elapsedTime:[NSNumber numberWithDouble:CMTimeGetSeconds([self.audioPlayer currentTime])]];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    
    [self.audioPlayer removeTimeObserver:self.timeObserverToken];
    self.timeObserverToken = nil;
    
    [[AVAudioSession sharedInstance] removeObserver:self forKeyPath:@"outputVolume"];
    
}


#pragma mark - UINavigationController

- (void)handleDoneAction:(UIBarButtonItem *)item {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - PlayerManager 

- (void)playSoundWithURL:(NSURL *)url {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[self.audioPlayer currentItem]];
    
    
    
    if ([self isPlaying] == YES) {
        [self.audioPlayer pause];
    }
    
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:url options:nil];
    AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:asset];
    self.audioPlayer = [AVPlayer playerWithPlayerItem:item];
    [self.audioPlayer play];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.audioPlayer currentItem]];
    
    
    
}

- (void)pause {
    CMTime pauseTime = [self.audioPlayer currentTime];
    NSLog(@"pauseTime = %f", CMTimeGetSeconds(pauseTime));
    [self.audioPlayer pause];
}

#pragma mark - NSNotificationCenter

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    [self handlePlayerFinishPlay];
}

#pragma mark - Compare Methods

- (BOOL)isPlaying {
    return ((self.audioPlayer.rate != 0) && (self.audioPlayer.error == nil));
}

- (BOOL)isSame:(NSURL *)url {
    
    AVURLAsset *newAsset = [AVURLAsset URLAssetWithURL:url options:nil];
    AVURLAsset *currentAsset = (AVURLAsset *)[self.audioPlayer.currentItem asset];
    
    return [currentAsset.URL isEqual:newAsset.URL] ? YES : NO;
}



#pragma mark - PlayerViewDelegate


- (void)tapOnPlayButton:(UIButton *)sender {
    
    self.playButton = sender;
    
    if ([self isPlaying] == YES) {
        [self pause];
        [sender setSelected:NO];
    } else {
        [[self audioPlayer] play];
        [sender setSelected:YES];
    }
    
}

- (void)tapOnBackwardButton:(UIButton *)sender {

    [self setPlayButtonCurrentState];
    
    if ([self playRandomSongForButtonState] == YES) {
        return ;
    }
    
    if (self.index != 0 || self.index > 0) {
        self.index -= 1;
        [self playSoundWithIndex:self.index];
    } else if (self.index == 0) {
        self.index = [self.arrayWithAudio count] - 1;
        [self playSoundWithIndex:self.index];
    }
}

- (void)tapOnForwardButton:(UIButton *)sender {
    
    [self setPlayButtonCurrentState];
    
    if ([self playRandomSongForButtonState] == YES) {
        return ;
    }

    if (self.index == ([self.arrayWithAudio count] - 1)) {
        self.index = 0;
        [self playSoundWithIndex:self.index];
    } else if (self.index == 0 || self.index < [self.arrayWithAudio count]) {
        self.index += 1;
        [self playSoundWithIndex:self.index];
    }
}



- (void)tapOnRepeatButton:(UIButton *)sender {
    [self saveButtonStateWithButton:sender andKey:@"repeatButtonState"];
}

- (void)tapOnRandomSongButton:(UIButton *)sender {
    [self saveButtonStateWithButton:sender andKey:@"randomButtonState"];
}

- (void)handleChangeVolumeLevel:(UISlider *)slider {
    
    MPVolumeView* volumeView = [[MPVolumeView alloc] init];
    
    UISlider* volumeViewSlider = nil;
    for (UIView *view in [volumeView subviews]){
        if ([view.class.description isEqualToString:@"MPVolumeSlider"]){
            volumeViewSlider = (UISlider*)view;
            break;
        }
    }
    
    [volumeViewSlider setValue:slider.value animated:YES];
}

- (void)handleChangeDurationValue:(UISlider *)slider {

    [self.audioPlayer seekToTime:CMTimeMakeWithSeconds(slider.value, 60000)];
}

#pragma mark - SomeMethods

- (void)playSoundWithIndex:(NSInteger )soundIndex {
    
    [self.audioPlayer seekToTime:kCMTimeZero];
    [self.audioPlayer removeTimeObserver:self.timeObserverToken];
    self.timeObserverToken = nil;
    
    AudioModel *indexSong = self.arrayWithAudio[soundIndex];
    NSURL *url = [[VKApi sharedInstance] getCurrentAudioURL:indexSong];
    [self playSoundWithURL:url];

    [self updateSongInformation:indexSong];
    
    [self startWithTimeObserver];
}

- (void)checkNowPlaying {    
    NSURL *currentUrl = [[VKApi sharedInstance] getCurrentAudioURL:self.song];
    BOOL isPlayingNow = [self isPlaying];
    BOOL isSongsSame = [self isSame:currentUrl];
    
    if (isPlayingNow == YES && isSongsSame == YES) {
        return ;
    } else if (isPlayingNow == NO && isSongsSame == YES) {
        return;
    } else {
        NSURL *url = [[VKApi sharedInstance] getCurrentAudioURL:self.song];
        [self playSoundWithURL:url];
    }
}

- (NSDictionary *)configureParamDictionary {
    
    NSNumber *buttonState = [NSNumber numberWithBool:[self isPlaying]];
    NSNumber *volumeLevel = [NSNumber numberWithFloat:[self volume]];
    NSString *fullTimeString = [NSString convertSecondsStringToCorrectFormat:self.song.duration];
    
    NSString *timeInSeconds = [NSString stringWithFormat:@"%f", CMTimeGetSeconds([self audioPlayer].currentTime)];
    NSString *currentTimeString = [NSString convertSecondsStringToCorrectFormat:timeInSeconds] ;
    
    
    NSDictionary *params = @{@"kArtist"                 : self.song.artist,
                             @"kTitle"                  : self.song.title,
                             @"kImageUrlString"         : @"nil",
                             @"kPlayButtonState"        : buttonState,
                             @"kFullSongTimeString"     : fullTimeString,
                             @"kCurrentTimeString"      : currentTimeString,
                             @"kMaxValueSliderDuration" : self.song.duration,
                             @"kCurrentVolumeLevel"     : volumeLevel};
    return params;
}

- (void)setPlayButtonCurrentState {
    if ([self isPlaying] == NO) {
        [self.playButton setSelected:YES];
    }
}

- (void)saveButtonStateWithButton:(UIButton *)sender andKey:(NSString *)key {
    
    if ([sender isSelected]) {
        [sender setSelected:NO];
    } else {
        [sender setSelected:YES];
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:sender.selected forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

-(BOOL)checkButtonStateForKey:(NSString *)key {
    return [[NSUserDefaults standardUserDefaults] boolForKey:key];
}

- (BOOL)playRandomSongForButtonState {
    if ([self checkButtonStateForKey:@"randomButtonState"] == YES) {
        NSInteger newRandomIndex = (NSInteger)arc4random_uniform((uint32_t)[self.arrayWithAudio count]);
        
        if (self.randomIndex == newRandomIndex) {
            newRandomIndex = (NSInteger)arc4random_uniform((uint32_t)[self.arrayWithAudio count]);
        }
        self.randomIndex = newRandomIndex;
        //arc4random() % [self.arrayWithAudio count];
        
        [self playSoundWithIndex:self.randomIndex];
        return YES;
    }
    return NO;
}

#pragma mark - ImageFromImageURL

-(void)getSongImage:(NSURL *)url withComplition:(void(^)(UIImage *image))complition {
    AVAsset *asset = [AVAsset assetWithURL:url];
    for (AVMetadataItem *metadataItem in asset.commonMetadata) {
        if ([metadataItem.commonKey isEqualToString:@"artwork"]) {
            UIImage *artworkImage = [UIImage imageWithData:(NSData *)metadataItem.value];
            complition(artworkImage);
        }
    }
    return ;
}

#pragma mark - Observers 


-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqual:@"outputVolume"]) {
        float newVolume = [[AVAudioSession sharedInstance] outputVolume];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"HandleUpdateVolumeLevel" object:[NSNumber numberWithFloat:newVolume]];
    }
}

- (void)startWithTimeObserver {
    
    CMTime interval = CMTimeMakeWithSeconds(1.0, NSEC_PER_SEC);
    self.timeObserverToken = [self.audioPlayer addPeriodicTimeObserverForInterval:interval queue:NULL usingBlock:^(CMTime time) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"HandleUpdateDurationTime" object:[NSNumber numberWithFloat:CMTimeGetSeconds(time)]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"HandleUpdateCurrentTimeLabel" object:[NSString convertSecondsStringToCorrectFormat:[NSString stringWithFormat:@"%f", CMTimeGetSeconds(time)]]];
        
    }];
}

- (void)updateSongInformation:(AudioModel *)newSong {
    
    NSString *newArtist = newSong.artist;
    NSString *newTitle = newSong.title;
    NSString *newTime = [NSString convertSecondsStringToCorrectFormat:newSong.duration];
    NSNumber *changeMaxDurationValue = [NSNumber numberWithFloat:newSong.duration.floatValue];
    
    NSDictionary *userInfo = @{@"newArtist"           : newArtist,
                               @"newTitle"            : newTitle,
                               @"newFullSongTime"     : newTime,
                               @"newMaxDurationValue" : changeMaxDurationValue};
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"HandleUpdateSongInfo" object:nil userInfo:userInfo];
    
    __weak PlayerViewController *weakSelf = self;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [weakSelf getSongImage:[[VKApi sharedInstance] getCurrentAudioURL:newSong] withComplition:^(UIImage *image) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"HandleArtworkUpdate" object:image];
            weakSelf.currentArtworkImage = image;
        }];
    });
    
    [self addNowPlayingInfo:newArtist songTitle:newTitle playbackDuratipn:[NSNumber numberWithDouble:newSong.duration.doubleValue] elapsedTime:[NSNumber numberWithDouble:CMTimeGetSeconds([self.audioPlayer currentTime])]];

    
}

#pragma mark PlayerManagerNotificationDelegate

- (void)handlePlayerFinishPlay {
    
    if ([self checkButtonStateForKey:@"repeatButtonState"] == YES) {
        [self.audioPlayer seekToTime:kCMTimeZero];
        [self.audioPlayer play];
    } else {
        [self tapOnForwardButton:nil];
    }

}

#pragma mark - MediaPlayerRemoteControls

- (void)addNowPlayingInfo:(NSString *)artist songTitle:(NSString *)title playbackDuratipn:(NSNumber *)duration elapsedTime:(NSNumber *)time {

    MPNowPlayingInfoCenter* mpic = [MPNowPlayingInfoCenter defaultCenter];
    mpic.nowPlayingInfo = @{MPMediaItemPropertyArtist                   : artist,
                            MPMediaItemPropertyTitle                    : title,
                            MPMediaItemPropertyPlaybackDuration         : duration,
                            MPNowPlayingInfoPropertyElapsedPlaybackTime : time};
}

- (void)addRemoteControls {
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    MPRemoteCommandCenter *commandCenter = [MPRemoteCommandCenter sharedCommandCenter];
    
    commandCenter.nextTrackCommand.enabled = YES;
    commandCenter.previousTrackCommand.enabled = YES;
    commandCenter.playCommand.enabled = YES;
    commandCenter.pauseCommand.enabled = YES;
    
    [commandCenter.nextTrackCommand addTarget:self action:@selector(nextTrackCommandSelector)];
    [commandCenter.previousTrackCommand addTarget:self action:@selector(previousTrackCommandSelector)];
    [commandCenter.playCommand addTarget:self action:@selector(playCommandSelector)];
    [commandCenter.pauseCommand addTarget:self action:@selector(pauseCommandSelector)];
}


- (void)nextTrackCommandSelector {
    [self tapOnForwardButton:nil];
}


- (void)previousTrackCommandSelector {
    [self tapOnBackwardButton:nil];
}


- (void)playCommandSelector {
    [self.audioPlayer play];
}


- (void)pauseCommandSelector {
    [self.audioPlayer pause];
}

#pragma mark - Dealloc

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[AVAudioSession sharedInstance] removeObserver:self forKeyPath:@"outputVolume"];
}


@end
