//
//  TabBarControllerNavigation.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 26/04/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SongsListTableController.h"
#import "DownloadsViewController.h"
#import "SettingsViewController.h"

@interface TabBarControllerNavigation : UITabBarController

@end
