//
//  AdvertisementMob.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 28/04/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "AdvertisementMob.h"

@interface AdvertisementMob () <GADInterstitialDelegate>

@property (strong, nonatomic) GADInterstitial *interstitial;

@end

@implementation AdvertisementMob 


- (instancetype)init {
    self = [super init];
    if (self) {
        self.interstitial = [self createAd];
    }
    return self;
}

- (GADInterstitial *)createAd {
    GADInterstitial *interstitial = [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-6650311325873368/1306479530"];
    interstitial.delegate = self;
    GADRequest *request = [GADRequest request];
    [interstitial loadRequest:request];
    return interstitial;
}


- (void)showAdWithViewController:(UIViewController *)currentVC {
    
    if (self.interstitial != nil) {
        
        if (self.interstitial.isReady == true) {
            [self.interstitial presentFromRootViewController:currentVC];
        } else {
            self.interstitial = [self createAd];
        }
        
    } else {
        self.interstitial = [self createAd];
    }
    
}


@end
