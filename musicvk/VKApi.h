//
//  VKApi.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 14/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EnumAndBlockHeader.h"
#import "AudioModel.h"
#import <UIKit/UIKit.h>


@interface VKApi : NSObject

@property (assign, nonatomic) AuthStates authState;
@property (assign, nonatomic) __block BOOL isConnectionReach;

+ (VKApi *)sharedInstance;

//Auth mehods
- (void)authWithName:(NSString *)userName andPassword:(NSString *)password andBlock:(AuthBlock)handleResult;
- (NSString *)loadToken;
- (BOOL)isUserAuthenticated;

//User

- (NSString *)userFullName;

//logout

- (void) forceLogout;

//Audios methods
- (void)getUserAudio:(ComplitionParseAudio)complition;
- (NSURL *)getCurrentAudioURL:(AudioModel *)song;
- (NSURL *)getDownloadedAudioWithObject:(AudioModel *)song;
- (void)addSongToUserAudiosWithAudioId:(NSString *)audioId andOwnerId:(NSString *)ownerId withComplition:(void(^)(BOOL complite))complition;

- (void) downloadAudioWithObject:(AudioModel *)song withComplition:(void(^)(BOOL complite))complition;

- (NSArray *)arrayWithAudioModels;

- (void)deleteSongFormDownloadsWithAudio:(AudioModel *)song;


//return array with 3 arrays
- (void)allSerchMethodsWithSerchString:(NSString *)searchStrgin andUserAudioArray:(NSArray *)userAudioArray andComplition:(void(^)(NSArray *resultArray))searchComplition;

//Advertisement

- (void)showAdvWithController:(UIViewController *)vc;

@end
