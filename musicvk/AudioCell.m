//
//  AudioCell.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 16/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "AudioCell.h"

@interface AudioCell ()

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;

@end

@implementation AudioCell

@synthesize playImageView;
@synthesize downloadButton, addButton;
@synthesize title, artist, songDurationLabel;
@synthesize activityIndicatorView;



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self initUIcomponentsWithReuseIdentifier:reuseIdentifier];
        //[self initUIcomponentsWithReuseIdentifier];
    }
    
    return self;
}



#pragma mark - InitUIcomponents



- (void)initUIcomponentsWithReuseIdentifier:(NSString *)reuseIdentifier {
    
    //init play button
    
    playImageView = [[UIImageView alloc] init];
    playImageView.translatesAutoresizingMaskIntoConstraints = NO;
    playImageView.image = [UIImage imageNamed:@"playFilled50Icon"];
    
    [self addSubview:playImageView];
    
    [playImageView.leftAnchor constraintEqualToAnchor:self.leftAnchor constant: 8.f].active = YES;
    [playImageView.centerYAnchor constraintEqualToAnchor:self.centerYAnchor].active = YES;
    [playImageView.widthAnchor constraintEqualToConstant:30.f].active = YES;
    [playImageView.heightAnchor constraintEqualToConstant:30.f].active = YES;
    
    songDurationLabel = [[UILabel alloc] init];
    songDurationLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [songDurationLabel setFont:[UIFont systemFontOfSize:12.f]];
    [songDurationLabel setTextColor:[UIColor lightGrayColor]];
    [self addSubview:songDurationLabel];
    
    [songDurationLabel.rightAnchor constraintEqualToAnchor:self.rightAnchor constant: -8.f].active = YES;
    [songDurationLabel.widthAnchor constraintEqualToConstant:35.f].active = YES;
    [songDurationLabel.centerYAnchor constraintEqualToAnchor:self.centerYAnchor].active = YES;
    
    UIView *tempViewForResize = songDurationLabel;
    
    if ([reuseIdentifier isEqualToString:@"searchCellIdentifier"] || [reuseIdentifier isEqualToString:@"songListIdentifier"]) {
    
    //init download button
    
        downloadButton = [UIButton buttonWithType:UIButtonTypeSystem];
        downloadButton.translatesAutoresizingMaskIntoConstraints = NO;
        [downloadButton setHidden:YES];
        downloadButton.tintColor = [UIColor lightGrayColor];
        [downloadButton setImage:[UIImage imageNamed:@"download50icon.png"] forState:UIControlStateNormal];
        downloadButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [downloadButton addTarget:self action:@selector(handleDownload:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:downloadButton];
    
        [downloadButton.rightAnchor constraintEqualToAnchor:songDurationLabel.leftAnchor constant: -8.f].active = YES;
        [downloadButton.centerYAnchor constraintEqualToAnchor:self.centerYAnchor].active = YES;
        [downloadButton.widthAnchor constraintEqualToConstant:30.f].active = YES;
        [downloadButton.heightAnchor constraintEqualToConstant:30.f].active = YES;
        
        // activity indicator
        
        activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
        activityIndicatorView.hidesWhenStopped = YES;
        [self addSubview:activityIndicatorView];
        
        [activityIndicatorView.rightAnchor constraintEqualToAnchor:songDurationLabel.leftAnchor constant: -8.f].active = YES;
        [activityIndicatorView.centerYAnchor constraintEqualToAnchor:self.centerYAnchor].active = YES;
        [activityIndicatorView.widthAnchor constraintEqualToConstant:32.f].active = YES;
        [activityIndicatorView.heightAnchor constraintEqualToConstant:32.f].active = YES;

        tempViewForResize = downloadButton;
    }
    
    if ([reuseIdentifier isEqualToString:@"searchCellIdentifier"]) {
        
        addButton = [UIButton buttonWithType:UIButtonTypeSystem];
        addButton.translatesAutoresizingMaskIntoConstraints = NO;
        addButton.tintColor = [UIColor lightGrayColor];
        [addButton setImage:[UIImage imageNamed:@"addSong50.png"] forState:UIControlStateNormal];
        addButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [addButton addTarget:self action:@selector(handleAddSound:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:addButton];
        
        [addButton.rightAnchor constraintEqualToAnchor:downloadButton.leftAnchor].active = YES;
        [addButton.centerYAnchor constraintEqualToAnchor:self.centerYAnchor].active = YES;
        [addButton.widthAnchor constraintEqualToConstant:30.f].active = YES;
        [addButton.heightAnchor constraintEqualToConstant:30.f].active = YES;
        
        tempViewForResize = addButton;
    }
    
    //constraints for textLabels
    
    title = [[UILabel alloc] init];
    title.translatesAutoresizingMaskIntoConstraints = NO;
    [title setFont:[UIFont systemFontOfSize:14.f]];
    [self addSubview:title];
    
    [title.topAnchor constraintEqualToAnchor:self.topAnchor constant:3.f].active = YES;
    [title.leftAnchor constraintEqualToAnchor:playImageView.rightAnchor constant:8.f].active = YES;
    [title.rightAnchor constraintEqualToAnchor:tempViewForResize.leftAnchor constant:-8.f].active = YES;

    artist = [[UILabel alloc] init];
    artist.translatesAutoresizingMaskIntoConstraints = NO;
    [artist setFont:[UIFont systemFontOfSize:12.f]];
    [self addSubview:artist];
    
    [artist.topAnchor constraintEqualToAnchor:title.bottomAnchor constant:3.f].active = YES;
    [artist.leftAnchor constraintEqualToAnchor:playImageView.rightAnchor constant:8.f].active = YES;
    [artist.rightAnchor constraintEqualToAnchor:tempViewForResize.leftAnchor constant: -8.f].active = YES;
}


#pragma mark Buttons Event 



- (void)handleDownload:(UIButton *)sender {
    [self.delegate downloadButtonTapped:sender inCell:self andActivityIndicator:activityIndicatorView];
}



- (void)handleAddSound:(UIButton *)sender {
    [self.delegate addAudioButtonTapped:sender inCell:self];
}


@end
