//
//  EnumAndBlockHeader.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 15/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#ifndef EnumAndBlockHeader_h
#define EnumAndBlockHeader_h

typedef enum authStateType {
    AUTH_SUCCESS,
    AUTH_SECOND_STEP_ERROR,
    AUTH_USER_DATA_ERROR,
    AUTH_INTERNET_CONNECTION_ERROR
} AuthStates;

typedef void (^ AuthBlock)(AuthStates authState, NSURL *redirectURL, NSDictionary *responce);
typedef void (^ ComplitionParseAudio) (NSArray *arrayWithAudios, NSError *error);

#endif /* EnumAndBlockHeader_h */
