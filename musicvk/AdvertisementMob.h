//
//  AdvertisementMob.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 28/04/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import <Foundation/Foundation.h>
@import GoogleMobileAds;

@interface AdvertisementMob : NSObject 

- (void)showAdWithViewController:(UIViewController *)currentVC;

@end
