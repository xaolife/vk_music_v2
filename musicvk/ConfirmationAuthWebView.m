//
//  ConfirmationAuthWebView.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 14/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "ConfirmationAuthWebView.h"

@interface ConfirmationAuthWebView ()

@end

@implementation ConfirmationAuthWebView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem.backBarButtonItem setTitle:@"Отмена"];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (id)initWithRequest:(NSURLRequest *)request {
    self = [super init];
    if (self) {
        _webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
        //_webView.delegate = self;
        [self.view addSubview:_webView];
        [_webView loadRequest:request];
    }
    return self;
}



@end
