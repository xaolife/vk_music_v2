//
//  PlayerViewController.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 17/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AudioModel;

@interface PlayerViewController : UIViewController

+ (PlayerViewController *)sharedPlayer;

@property (strong, nonatomic) AudioModel *song;
@property (strong, nonatomic) NSArray *arrayWithAudio;
@property (assign, nonatomic) NSInteger index;

- (BOOL)isPlaying;
- (BOOL)isSame:(NSURL *)url;

@end
