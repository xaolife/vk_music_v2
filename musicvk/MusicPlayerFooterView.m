//
//  MusicPlayerFooterView.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 17/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "MusicPlayerFooterView.h"

@implementation MusicPlayerFooterView

@synthesize playPauseButton;
@synthesize songTitle, artistName;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self initMusicView];
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleFooterViewCanges:) name:@"HandleFooterViewCanges" object:nil];
    }
    
    return self;
}

- (void)initMusicView {
    
    [self setBackgroundColor:[UIColor colorWithRed:(105/255.f) green:(105/255.f) blue:(105/255.f) alpha:0.9f]];
    
    playPauseButton = [[UIButton alloc] init];
    playPauseButton.translatesAutoresizingMaskIntoConstraints = NO;
    [playPauseButton setImage:[UIImage imageNamed:@"play50icon.png"] forState:UIControlStateNormal];
    [playPauseButton setImage:[UIImage imageNamed:@"pause50icon.png"] forState:UIControlStateSelected];
    [playPauseButton addTarget:self action:@selector(handlePlay:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:playPauseButton];
    
    [playPauseButton.centerYAnchor constraintEqualToAnchor:self.centerYAnchor].active = YES;
    [playPauseButton.leftAnchor constraintEqualToAnchor:self.leftAnchor constant:8.f].active = YES;
    [playPauseButton.widthAnchor constraintEqualToConstant:32.f].active = YES;
    [playPauseButton.heightAnchor constraintEqualToConstant:32.f].active = YES;
    
    songTitle = [[UILabel alloc] init];
    songTitle.text = @"Asferiuwiugehg";
    [songTitle setTextColor:[UIColor whiteColor]];
    [songTitle setFont:[UIFont systemFontOfSize:16.f]];
    songTitle.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:songTitle];
    
    
    [songTitle.leftAnchor constraintEqualToAnchor:playPauseButton.rightAnchor constant:4.f].active = YES;
    [songTitle.topAnchor constraintEqualToAnchor:self.topAnchor constant:8.f].active = YES;
    [songTitle.widthAnchor constraintEqualToConstant:self.frame.size.width - 48.f].active = YES;
    
    artistName = [[UILabel alloc] init];
    artistName.text = @"Qdajidjiwofjerif";
    [artistName setTextColor:[UIColor whiteColor]];
    [artistName setFont:[UIFont systemFontOfSize:14.f]];
    artistName.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:artistName];
    
    
    [artistName.leftAnchor constraintEqualToAnchor:playPauseButton.rightAnchor constant:4.f].active = YES;
    [artistName.topAnchor constraintEqualToAnchor:songTitle.bottomAnchor].active = YES;
    [artistName.widthAnchor constraintEqualToConstant:self.frame.size.width - 48.f].active = YES;
    
}

- (void)currentButtonState:(BOOL)state {
    NSLog(@"state %@", state ? @"yes" : @"no");
    [playPauseButton setSelected:state];
}

- (void)handlePlay:(UIButton *)sender {
    
    NSLog(@"play from footer = %@", sender);
    
}


@end
