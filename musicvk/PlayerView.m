//
//  PlayerView.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 20/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "PlayerView.h"

@interface PlayerView ()

@property (strong, nonatomic) UISlider *songDurationSlider;
@property (strong, nonatomic) UISlider *volumeSlider;
@property (strong, nonatomic) UILabel *currentSongTime;
@property (strong, nonatomic) UILabel *fullSongTime;
@property (strong, nonatomic) UILabel *artistName;
@property (strong, nonatomic) UILabel *songTitle;
@property (strong, nonatomic) UIImageView *artworkImage;

@end

@implementation PlayerView
@synthesize songDurationSlider, volumeSlider,
               currentSongTime, fullSongTime,
                    artistName, songTitle, artworkImage;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addAllObservers];
    }
    return self;
}

- (void)changeInterfaceWithParams:(NSDictionary *)params {
    [self initUIcomonentsWithArtist:[params  objectForKey : @"kArtist"]
                              title:[params  objectForKey : @"kTitle"]
                           imageUrl:[params  objectForKey : @"kImageUrlString"]
                    playButtonState:[[params objectForKey : @"kPlayButtonState"] boolValue]
                 fullSongTimeString:[params  objectForKey : @"kFullSongTimeString"]
              currentSongTimeString:[params  objectForKey : @"kCurrentTimeString"]
                  maxValueForSlider:[[params objectForKey : @"kMaxValueSliderDuration"] floatValue]
                        volumeLevel:[[params objectForKey : @"kCurrentVolumeLevel"] floatValue]];
}


- (void)initUIcomonentsWithArtist:(NSString *)  artist
                            title:(NSString *)  title
                         imageUrl:(NSString *)  imageUrlString
                  playButtonState:(BOOL)        buttonState
               fullSongTimeString:(NSString *)  fullSongTimeString
            currentSongTimeString:(NSString *)  currentTimeSongString
                maxValueForSlider:(float )      maxValueSliderDuration
                      volumeLevel:(float )      volumeLevel {
    
    UIImageView *nilSongImge = [[UIImageView alloc] init];
    nilSongImge.image = [UIImage imageNamed:@"nilSongImage.png"];
    [nilSongImge setBackgroundColor:[UIColor lightGrayColor]];
    nilSongImge.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:nilSongImge];
    
    //constraints for nil song image
    
    [nilSongImge.topAnchor constraintEqualToAnchor:self.topAnchor].active = YES;
    [nilSongImge.leftAnchor constraintEqualToAnchor:self.leftAnchor].active = YES;
    [nilSongImge.rightAnchor constraintEqualToAnchor:self.rightAnchor].active = YES;
    [nilSongImge.widthAnchor constraintEqualToConstant:self.frame.size.width].active = YES;
    [nilSongImge.heightAnchor constraintEqualToConstant:self.frame.size.height * 7/12.f].active = YES;
    
    NSLog(@"view %@", NSStringFromCGSize(self.frame.size));
    
    artworkImage = [[UIImageView alloc] init];
    artworkImage.translatesAutoresizingMaskIntoConstraints = NO;
    [nilSongImge addSubview:artworkImage];
    
    [artworkImage.centerXAnchor constraintEqualToAnchor:nilSongImge.centerXAnchor].active = YES;
    [artworkImage.centerYAnchor constraintEqualToAnchor:nilSongImge.centerYAnchor].active = YES;
    [artworkImage.widthAnchor constraintEqualToAnchor:nilSongImge.widthAnchor].active = YES;
    [artworkImage.heightAnchor constraintEqualToConstant:(self.frame.size.height * 7/12.f) - 16.f].active = YES;
    
    UIView *containerView = [[UIView alloc] init];
    [containerView setBackgroundColor:[UIColor whiteColor]];
    containerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:containerView];
    
    [containerView.topAnchor constraintEqualToAnchor:nilSongImge.bottomAnchor].active = YES;
    [containerView.widthAnchor constraintEqualToConstant:self.frame.size.width].active = YES;
    [containerView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor].active = YES;
    
    songDurationSlider = [[UISlider alloc] init];
    [songDurationSlider addTarget:self action:@selector(handleDurationValueChanged:) forControlEvents:UIControlEventValueChanged];
    [containerView addSubview:songDurationSlider];
    songDurationSlider.minimumTrackTintColor = [UIColor colorWithRed:(70/255.f) green:(70/255.f) blue:(70/255.f) alpha:1.f];
    [songDurationSlider setBackgroundColor:[UIColor clearColor]];
    songDurationSlider.minimumValue = 0.0;
    songDurationSlider.maximumValue = maxValueSliderDuration;
    songDurationSlider.continuous = YES;
    songDurationSlider.value = 0.0;
    songDurationSlider.translatesAutoresizingMaskIntoConstraints = NO;
    
    currentSongTime = [[UILabel alloc] init];
    currentSongTime.text = currentTimeSongString;
    currentSongTime.translatesAutoresizingMaskIntoConstraints = NO;
    [currentSongTime setFont:[UIFont systemFontOfSize:12.f]];
    [containerView addSubview:currentSongTime];
    
    fullSongTime = [[UILabel alloc] init];
    fullSongTime.text = fullSongTimeString;
    fullSongTime.translatesAutoresizingMaskIntoConstraints = NO;
    [fullSongTime setFont:[UIFont systemFontOfSize:12.f]];
    [containerView addSubview:fullSongTime];
    
    //constraints for current song time
    
    [currentSongTime.leftAnchor constraintEqualToAnchor:containerView.leftAnchor constant:8.f].active = YES;
    [currentSongTime.centerYAnchor constraintEqualToAnchor:songDurationSlider.centerYAnchor].active = YES;
    
    // constraints for full song time
    
    [fullSongTime.rightAnchor constraintEqualToAnchor:containerView.rightAnchor constant:-8.f].active = YES;
    [fullSongTime.centerYAnchor constraintEqualToAnchor:songDurationSlider.centerYAnchor].active = YES;
    
    //constraints for songDuratioSlider
    
    [songDurationSlider.topAnchor constraintEqualToAnchor:containerView.topAnchor constant:8.f].active = YES;
    [songDurationSlider.leftAnchor constraintEqualToAnchor:currentSongTime.rightAnchor constant:8.f].active = YES;
    [songDurationSlider.rightAnchor constraintEqualToAnchor:fullSongTime.leftAnchor constant:-8.f].active = YES;
    
    
    UIButton *repeatButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [repeatButton addTarget:self action:@selector(handleRepeatSongAction:) forControlEvents:UIControlEventTouchUpInside];
    [repeatButton setImage:[UIImage imageNamed:@"repeat50dsel"] forState:UIControlStateNormal];
    [repeatButton setImage:[UIImage imageNamed:@"repeat50SelBlue"] forState:UIControlStateSelected];
    [repeatButton setSelected:[[NSUserDefaults standardUserDefaults] boolForKey:@"repeatButtonState"]];
    repeatButton.translatesAutoresizingMaskIntoConstraints = NO;
    [containerView addSubview:repeatButton];
    
    [repeatButton.topAnchor constraintEqualToAnchor:songDurationSlider.bottomAnchor constant:4.f].active = YES;
    [repeatButton.leftAnchor constraintEqualToAnchor:containerView.leftAnchor constant:8.f].active = YES;
    
    UIButton *randomSongButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [randomSongButton addTarget:self action:@selector(handleRandomSongAction:) forControlEvents:UIControlEventTouchUpInside];
    [randomSongButton setImage:[UIImage imageNamed:@"shuffle50dsel"] forState:UIControlStateNormal];
    [randomSongButton setImage:[UIImage imageNamed:@"shuffle50SelBlue"] forState:UIControlStateSelected];
    [randomSongButton setSelected:[[NSUserDefaults standardUserDefaults] boolForKey:@"randomButtonState"]];
    randomSongButton.translatesAutoresizingMaskIntoConstraints = NO;
    [containerView addSubview:randomSongButton];
    
    [randomSongButton.rightAnchor constraintEqualToAnchor:containerView.rightAnchor constant:-8.f].active = YES;
    [randomSongButton.topAnchor constraintEqualToAnchor:songDurationSlider.bottomAnchor constant:4.f].active = YES;
    
    artistName = [[UILabel alloc] init];
    artistName.text = artist;
    [artistName setFont:[UIFont systemFontOfSize:16.f]];
    artistName.translatesAutoresizingMaskIntoConstraints = NO;
    [containerView addSubview:artistName];
    
    songTitle = [[UILabel alloc] init];
    songTitle.text = title;
    [songTitle setFont:[UIFont systemFontOfSize:14.f]];
    songTitle.translatesAutoresizingMaskIntoConstraints = NO;
    [containerView addSubview:songTitle];
    
    UIButton *backSongButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [backSongButton addTarget:self action:@selector(handleBackwardAction:) forControlEvents:UIControlEventTouchUpInside];
    [backSongButton setImage:[UIImage imageNamed:@"backwardFilled100"] forState:UIControlStateNormal];
    [backSongButton setTintColor:[UIColor blackColor]];
    backSongButton.translatesAutoresizingMaskIntoConstraints = NO;
    [containerView addSubview:backSongButton];
    
    UIButton *playSongButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [playSongButton addTarget:self action:@selector(handlePlayAction:) forControlEvents:UIControlEventTouchUpInside];
    [playSongButton setTintColor:[UIColor blackColor]];
    [playSongButton setImage:[UIImage imageNamed:@"playFilled100"] forState:UIControlStateNormal];
    [playSongButton setImage:[UIImage imageNamed:@"pauseFilled100"] forState:UIControlStateSelected];
    [playSongButton setSelected:buttonState];
    playSongButton.translatesAutoresizingMaskIntoConstraints = NO;
    [containerView addSubview:playSongButton];
    
    UIButton *forwardSongButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [forwardSongButton addTarget:self action:@selector(handleForwardAction:) forControlEvents:UIControlEventTouchUpInside];
    [forwardSongButton setImage:[UIImage imageNamed:@"forwardFilled100"] forState:UIControlStateNormal];
    [forwardSongButton setTintColor:[UIColor blackColor]];
    forwardSongButton.translatesAutoresizingMaskIntoConstraints = NO;
    [containerView addSubview:forwardSongButton];
    
    volumeSlider = [[UISlider alloc] init];
    [volumeSlider addTarget:self action:@selector(handleVolumeLevelChanged:) forControlEvents:UIControlEventValueChanged];
    [containerView addSubview:volumeSlider];
    [volumeSlider setBackgroundColor:[UIColor clearColor]];
    volumeSlider.minimumTrackTintColor = [UIColor colorWithRed:(70/255.f) green:(70/255.f) blue:(70/255.f) alpha:1.f];
    //[UIColor colorWithRed:(70/255.f) green:(70/255.f) blue:(70/255.f) alpha:1.f];
    //[UIColor colorWithRed:(192 / 255.f) green:(57 / 255.f) blue:(43 / 255.f) alpha:1.f];
    volumeSlider.minimumValue = 0.0f;
    volumeSlider.maximumValue = 1.0f;
    volumeSlider.continuous = YES;
    volumeSlider.value = volumeLevel;
    [volumeSlider setMinimumValueImage:[UIImage imageNamed:@"lowVolume50.png"]];
    [volumeSlider setMaximumValueImage:[UIImage imageNamed:@"highVolume50.png"]];
    volumeSlider.translatesAutoresizingMaskIntoConstraints = NO;
    
    UIView *spaceBetweenArtistAndButton = [[UIView alloc] init];
    spaceBetweenArtistAndButton.translatesAutoresizingMaskIntoConstraints = NO;
    [containerView addSubview:spaceBetweenArtistAndButton];
    
    UIView *spaceBetweenButtonVol = [[UIView alloc] init];
    spaceBetweenButtonVol.translatesAutoresizingMaskIntoConstraints = NO;
    [containerView addSubview:spaceBetweenButtonVol];
    
    //constraints for artistName
    
    [artistName.topAnchor constraintEqualToAnchor:repeatButton.bottomAnchor].active = YES;
    [artistName.centerXAnchor constraintEqualToAnchor:containerView.centerXAnchor].active = YES;
    
    //constraints for artistName
    
    [songTitle.topAnchor constraintEqualToAnchor:artistName.bottomAnchor constant:4.f].active = YES;
    [songTitle.centerXAnchor constraintEqualToAnchor:containerView.centerXAnchor].active = YES;
    
    [spaceBetweenArtistAndButton.topAnchor constraintEqualToAnchor:songTitle.bottomAnchor].active = YES;
    [spaceBetweenArtistAndButton.widthAnchor constraintEqualToConstant:self.frame.size.width].active = YES;
    [spaceBetweenArtistAndButton.heightAnchor constraintEqualToAnchor:spaceBetweenButtonVol.heightAnchor].active = YES;
    
    //constraints for back button
    
    [playSongButton.topAnchor constraintEqualToAnchor:spaceBetweenArtistAndButton.bottomAnchor].active = YES;
    [playSongButton.centerXAnchor constraintEqualToAnchor:containerView.centerXAnchor].active = YES;
    
    //constrains for back button
    
    [backSongButton.rightAnchor constraintEqualToAnchor:playSongButton.leftAnchor constant:-16.f].active = YES;
    [backSongButton.leftAnchor constraintEqualToAnchor:containerView.leftAnchor].active = YES;
    [backSongButton.topAnchor constraintEqualToAnchor:spaceBetweenArtistAndButton.bottomAnchor].active = YES;
    
    //constraints for forward button
    
    [forwardSongButton.leftAnchor constraintEqualToAnchor:playSongButton.rightAnchor constant:16.f].active = YES;
    [forwardSongButton.rightAnchor constraintEqualToAnchor:containerView.rightAnchor].active = YES;
    [forwardSongButton.topAnchor constraintEqualToAnchor:spaceBetweenArtistAndButton.bottomAnchor].active = YES;
    
    
    [spaceBetweenButtonVol.topAnchor constraintEqualToAnchor:playSongButton.bottomAnchor].active = YES;
    [spaceBetweenButtonVol.widthAnchor constraintEqualToConstant:self.frame.size.width].active = YES;
    [spaceBetweenButtonVol.bottomAnchor constraintEqualToAnchor:volumeSlider.topAnchor].active = YES;
    
    // constraints for volume slider
    
    [volumeSlider.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:-8.f].active = YES;
    [volumeSlider.rightAnchor constraintEqualToAnchor:self.rightAnchor constant:-8.f].active = YES;
    [volumeSlider.leftAnchor constraintEqualToAnchor:self.leftAnchor constant:8.f].active = YES;
    
}

#pragma mark - NSNotificationCenter

- (void)addAllObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUpdateDurationTime:)
                                                 name:@"HandleUpdateDurationTime"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUpdateCurrentTimeLabel:)
                                                 name:@"HandleUpdateCurrentTimeLabel"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUpdateVolumeLevel:)
                                                 name:@"HandleUpdateVolumeLevel"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUpdateSongInfo:)
                                                 name:@"HandleUpdateSongInfo"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleArtworkUpdate:)
                                                 name:@"HandleArtworkUpdate"
                                               object:nil];
    
}

- (void)handleUpdateDurationTime:(NSNotification *)notificetion {
    songDurationSlider.value = [(NSNumber *)notificetion.object floatValue];
}

- (void)handleUpdateCurrentTimeLabel:(NSNotification *)notificetion {
    currentSongTime.text = (NSString *)notificetion.object;
}

- (void)handleUpdateVolumeLevel:(NSNotification *)notificetion {
    volumeSlider.value = [(NSNumber *)notificetion.object floatValue];
}

- (void)handleUpdateSongInfo:(NSNotification *)notification {
    
    NSString *newFullSongTime = [notification.userInfo objectForKey:@"newFullSongTime"];
    NSString *newArtist = [notification.userInfo objectForKey:@"newArtist"];
    NSString *newTitle = [notification.userInfo objectForKey:@"newTitle"];
    float newMaxDurationValueForSlider = [[notification.userInfo objectForKey:@"newMaxDurationValue"] floatValue];
    
    fullSongTime.text = newFullSongTime;
    artistName.text = newArtist;
    songTitle.text = newTitle;
    songDurationSlider.maximumValue = newMaxDurationValueForSlider;
    
}

- (void)handleArtworkUpdate:(NSNotification *)notification {
    
    if ((UIImage *)notification.object != nil) {
        [artworkImage setImage:(UIImage *)notification.object];
        [UIView animateWithDuration:0.2
                         animations:^{[artworkImage setHidden:NO];}
                         completion:nil];
    } else {
        [UIView animateWithDuration:0.2
                         animations:^{[artworkImage setHidden:YES];}
                         completion:nil];
    }
    
}

#pragma mark - Actions for for PlayerViewDelegate

- (void)handleRandomSongAction:(UIButton *)sender {
    [self.delegate tapOnRandomSongButton:sender];
}

- (void)handleRepeatSongAction:(UIButton *)sender {
    [self.delegate tapOnRepeatButton:sender];
}

- (void)handleBackwardAction:(UIButton *)sender {
    [self.delegate tapOnBackwardButton:sender];
}

- (void)handlePlayAction:(UIButton *)sender {
    [self.delegate tapOnPlayButton:sender];
}

- (void)handleForwardAction:(UIButton *)sender {
    [self.delegate tapOnForwardButton:sender];
}

- (void)handleDurationValueChanged:(UISlider *)slider {
    [self.delegate handleChangeDurationValue:slider];
}

- (void)handleVolumeLevelChanged:(UISlider *)slider {
    [self.delegate handleChangeVolumeLevel:slider];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
