//
//  CoreDataManager.m
//  musicvk
//
//  Created by  Dmitry Babinsky on 24/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import "CoreDataManager.h"
#import "CDMAudio+CoreDataProperties.h"
#import <QuartzCore/QuartzCore.h>


@implementation CoreDataManager
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;




- (void)storageSongWith:(AudioModel *)aModel andNewfilePath:(NSString *)filePath {
    
    CDMAudio *cdmAudio = [NSEntityDescription insertNewObjectForEntityForName:@"CDMAudio" inManagedObjectContext:self.managedObjectContext];
    
    
    cdmAudio.title = aModel.title;
    cdmAudio.artist = aModel.artist;
    cdmAudio.duration = [NSNumber numberWithInt:aModel.duration.intValue];
    cdmAudio.aid = [NSNumber numberWithInt:aModel.aid.intValue];
    cdmAudio.url = filePath;
    
    NSError *error = nil;
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"%@", [error localizedDescription]);
    }
    
}

- (NSArray *)objectsFromCDMAudioWithPredicate:(NSPredicate *)predicate {
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *description = [NSEntityDescription entityForName:@"CDMAudio" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:description];
    
    if (predicate != nil) {
        [request setPredicate:predicate];
    }
    
    NSError *requestError = nil;
    
    NSArray *resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    
    if (requestError) {
        NSLog(@"Error = %@", [requestError localizedDescription]);
    }
    
    return resultArray;
}

- (NSArray *)arrayWithAudioModelsFromCDM {
    
    NSArray *arrayWithObjects = [self objectsFromCDMAudioWithPredicate:nil];
    
    return [self parseCDMAudioToAudioModelWithArray:arrayWithObjects];
}

- (NSArray *)parseCDMAudioToAudioModelWithArray:(NSArray *)arrayWithObjects {
 
    NSMutableArray *resultedArray = [[NSMutableArray alloc] init];
    
    for (CDMAudio *song in arrayWithObjects) {
        AudioModel *audio = [[AudioModel alloc] init];
        audio.title = song.title;
        audio.artist = song.artist;
        audio.url = song.url;
        audio.duration = [NSString stringWithFormat:@"%i", song.duration.intValue];
        audio.aid = [NSString stringWithFormat:@"%i", song.aid.intValue];
        [resultedArray addObject:audio];
    }
    return resultedArray;
}

- (NSArray *)searchInDownloadedSongsWithSearchString:(NSString *)searchString {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title contains[cd] %@", searchString];
    NSArray *searchResult = [self objectsFromCDMAudioWithPredicate:predicate];
    
    return [self parseCDMAudioToAudioModelWithArray:searchResult];
}

- (void)deleteFromCoreData:(AudioModel *)audio {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"CDMAudio" inManagedObjectContext:self.managedObjectContext]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"url == %@", audio.url];
    [fetchRequest setPredicate:predicate];
    
    NSError* error = nil;
    NSArray* results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    CDMAudio *currentObject = results[0];
    
    [self.managedObjectContext deleteObject:currentObject];
    
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"%@", [error localizedDescription]);
    }
    
}


#pragma mark - Core Data stack

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil){
        return _managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"MusicCoreDataModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if(_persistentStoreCoordinator != nil){
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"MusicCoreDataModel.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
    if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]){
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    if(_managedObjectContext != nil){
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if(coordinator != nil){
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    
    return _managedObjectContext;
}

- (NSURL *)applicationDocumentsDirectory{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)saveContext {
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    if(managedObjectContext != nil) {
        if([managedObjectContext hasChanges] && ![managedObjectContext save:&error]){
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}







@end
