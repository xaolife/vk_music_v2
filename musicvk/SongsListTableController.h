//
//  SongsListTableController.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 15/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AudioModel.h"
#import "AudioCell.h"

@interface SongsListTableController : UITableViewController


- (void)configureCell:(AudioCell *)cell withAudio:(AudioModel *)audio;

@end
