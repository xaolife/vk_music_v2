//
//  CoreDataManager.h
//  musicvk
//
//  Created by  Dmitry Babinsky on 24/03/17.
//  Copyright © 2017  Dmitry Babinsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AudioModel.h"

@interface CoreDataManager : NSObject

@property (nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

- (void)storageSongWith:(AudioModel *)aModel andNewfilePath:(NSString *)filePath;
- (NSArray *)arrayWithAudioModelsFromCDM;

//return an AudioModel array objects
- (NSArray *)searchInDownloadedSongsWithSearchString:(NSString *)searchString;

- (void)deleteFromCoreData:(AudioModel *)audio;

@end
